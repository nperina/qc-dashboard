<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException; 

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use JWTAuth;
use Tymon\Exceptions\JWTException;

use Illuminate\Foundation\Bootstrap\ConfigureLogging;
use Log;
use App\User;


class ApiController extends Controller {

	public function __construct()
   	{
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth', ['except' => ['authenticate']]);
   	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all();
		return $users; 
	}

	public function createBatch(Request $request)
	{
		//
		Log::info("\r\n Create batch api: \r\n task_id :" . $request->task_id . "\r\n project_id: " . $request->project_id .
			"\r\n batch name: " . $request->batch_name . "\r\n batch size: " . $request->batch_size);
		$unit = DB::table('tasks AS t')
                    ->join('references AS ref', 't.unit_type', '=', 'ref.id')
                    ->select('ref.name')
                    ->where('ref.types', 'UNIT')
                    ->where('t.id',  $request->task_id)
                    ->distinct()->get();
        Log::info("\r\n unit: " . $unit[0]->name); 
		try {
			
			$batch = \App\Batch::create(['project_id' => (int) $request->project_id, 
	            'batch_name' => $request->batch_name,
	            'task_id' =>  (int) $request->task_id,
	            'unit' => $unit[0]->name,
	            'batch_size' => (int) $request->batch_size,
        	]);
        	$batch_id = $batch->id;
        	return response()->json(['batch_id' => $batch_id]);

		} catch(QueryException $e){
			$info = $e->errorInfo; 
			return response()->json(['error' => $info], 401);
		}
	}

	public function createError(Request $request)
	{
		try {

			$error = new \App\Error;
            $error->batch_id = (int) $request->batch_id;
            $error->round_number = (int) $request->round_number;
            if ($request->subtask_id == null){
            	$error->subtask_id = null; 
            }
            else {
            	$error->subtask_id = (int) $request->subtask_id;
            }
            if ($request->employee_id == null){
            	$error->employee_id =  null; 
            }
            else {
            	$error->employee_id = (int) $request->employee_id;
            }           
            if ($request->unit_number == null){
            	$error->unit_number = null; 
            }
            else {
            	$error->unit_number = (int) $request->unit_number;
            }
            $error->description =  $request->error_description;
            $error->save(); 
        	return response()->json(['success' => true]);

		} catch(QueryException $e){
			$info = $e->errorInfo; 
			return response()->json(['error' => $info], 401);
		}
	}



	public function createRound(Request $request)
	{
		$total = (int) $request->sample_size;

        // accuracy of this qc round
        $accuracy = (( $total - (int)$request->units_in_error ) / $total )* 100;

        //target accuracy of this qc round
        $target_accuracy = (double) $request->target_accuracy; 

        // if accuracy > target accuracy, pass; else fail;
        $status; 
        if ($accuracy >= $target_accuracy) $status = true;
        else $status = false;  
		try {
			
			 $round = \App\qc_round::create(['batch_id' => (int) $request->batch_id, 
	            'description' => $request->round_description,
	            'sample_size' =>  (int) $request->sample_size,
	            'units_found_in_error' => (int) $request->units_in_error,
	            'accuracy' => $accuracy,
	            'pass?' => $status,
	            'target_accuracy' => (double) $request->target_accuracy
        	]);
        	return response()->json(['round_number' => $round->id]);

		} catch(QueryException $e){
			$info = $e->errorInfo; 
			return response()->json(['error' => $info], 401);
		}
	}

	public function authenticate(Request $request)
    {
    	Log::info("\r\n Inside ApiController Authenticate Function \r\n ");
        $credentials = $request->only('username', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

	

}
