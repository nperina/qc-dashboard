<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller; 
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Foundation\Bootstrap\ConfigureLogging;
use Log;

class ProjectController extends Controller
{
    /* returns home page */ 
    public function getHome(Request $request)
    {

        //$batches = \App\Batch::select('batchnae');
        $batches =  DB::select(DB::raw("SELECT qb.batch_id, qb.batch_name, off.office_abbr, qb.project_id, pr.name AS project_name, qb.task_id, tk.name AS task_name, qb.unit, qb.batch_size, a.num_rounds FROM (
            (SELECT batch_id, COUNT(*) AS `num_rounds` FROM qc_rounds GROUP BY batch_id) a 
            LEFT JOIN 
            qc_batches AS qb ON qb.batch_id = a.batch_id 
            LEFT JOIN 
            projects pr
            ON qb.project_id = pr.id 
            LEFT JOIN
            lu_office off
            ON pr.projects_location = off.id
            LEFT JOIN 
            tasks tk 
            ON qb.task_id = tk.id 
            );" ));
        return view('home', ['batches' => json_encode($batches, JSON_UNESCAPED_SLASHES)]);

    }

    public function download(Request $request)
    {
        Log::info("\r\n DOWNLOAD: \r\n office: ..." . $request->office . "...");
        Log::info("\r\n Download: \r\n project: ..." . $request->project . "...");
        Log::info("\r\n Download: \r\n TZ: ..." . $request->tz . "...");
        $proj = $request->project;
        $off = $request->office; 
        if (ctype_digit($request->tz)){
            $tz_off = (int) $request->tz;
        }
        else {
            $tz_off = 0; 
        }
        if ($tz_off == 0){
            $tz_off_str = '+0:00';
        }
        elseif ($tz_off > 0) {
            $tz_off_str = "+" . $tz_off . ":00";
        }
        else {
            $tz_off_str = (string) $tz_off . ":00"; 
        }
        $tz_utc = "+0:00";

        $download_name = "";

        if (($proj == "all") && ($off == "all")){
            $download_name = "AllProjects_QcRounds.csv";
            $rounds = DB::table('qc_rounds AS R')
                    ->join('qc_batches AS B', 'B.batch_id', '=', 'R.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->join('lu_office AS off', 'off.id' ,'=', 'pr.projects_location')
                    ->select('R.batch_id as BATCH_ID', 'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER', 'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME','off.office_abbr AS OFFICE', 'tk.name AS TASK_NAME', 'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL',DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `DATE (UTC " . $tz_off_str . ")`"))
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 
        }
        if (($proj == "all") && ($off != "all")) {
            $download_name = $off . "Projects_QcRounds.csv";

            if ($off == "PNH") $id = 1;
            if ($off == "VTE") $id = 2;
            if ($off == "NBO") $id = 3;

            $rounds = DB::table('qc_rounds AS R')
                    ->join('qc_batches AS B', 'B.batch_id', '=', 'R.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->join('lu_office AS off', 'off.id', '=', 'pr.projects_location')
                    ->select('R.batch_id as BATCH_ID', 'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER', 'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME', 'off.office_abbr AS OFFICE', 'tk.name AS TASK_NAME', 'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL',DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `DATE (UTC " . $tz_off_str . ")`"))
                    ->where('pr.projects_location', $id)
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 
        }

        if ($proj != "all"){
            
            $rounds = DB::table('qc_rounds AS R')
                    ->join('qc_batches AS B', 'B.batch_id', '=', 'R.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->join('lu_office AS off', 'off.id' ,'=', 'pr.projects_location')
                    ->select('R.batch_id as BATCH_ID', 'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER', 'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME', 'off.office_abbr AS OFFICE','tk.name AS TASK_NAME', 'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL',DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `DATE (UTC " . $tz_off_str . ")`"))
                    ->where('pr.id', $proj)
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 
            $proj_name = $rounds[0]->PROJECT_NAME;
            $download_name = $proj_name . "_QcRounds.csv";
        }

        $errors = DB::table('qc_errors AS E')
                    ->leftJoin('employee AS emp', 'E.employee_id', '=', 'emp.id')
                    ->leftJoin('subtasks AS st', 'E.subtask_id', '=', 'st.subtask_id')
                    ->select('E.batch_id AS BATCH_ID', 'E.round_number AS ROUND_NUMBER', 'emp.employee_id as EMPLOYEE_ID', 'E.subtask_id AS SUBTASK_ID', 'st.subtask_name AS SUBTASK_NAME', 'emp.given_name AS GIVEN_NAME', 'emp.family_name AS FAMILY_NAME', 'E.description AS DESCRIPTION', 'e.unit_number AS UNIT_NUMBER')
                    ->get(); 
        Log::info("\r\n \r\n ROUNDS: " . serialize($rounds) . "\r\n \r\n ");
        foreach($rounds as $round){
                $batch_id = $round->BATCH_ID;
                $round_number = $round->ROUND_NUMBER;
                $this_errors = array_filter($errors, function($item) use($batch_id, $round_number) { 
                    return ( $item->BATCH_ID == $batch_id && $item->ROUND_NUMBER == $round_number );
                });
                $round->ERRORS_SUBTASKS= 'Subtasks: ';
                $round->ERRORS_EMPLOYEES = 'Employees: ';
                $round->ERRORS_NOTES = 'Notes:';
                foreach($this_errors as $error){
                    if ($error->SUBTASK_ID == null){
                        $round->ERRORS_SUBTASKS=  $round->ERRORS_SUBTASKS  . "None, ";

                    }
                    else {
                        $round->ERRORS_SUBTASKS=  $round->ERRORS_SUBTASKS . (string) $error->SUBTASK_ID . " | " . $error->SUBTASK_NAME . ", ";
                    }
                    if ($error->EMPLOYEE_ID == null){
                         $round->ERRORS_EMPLOYEES = $round->ERRORS_EMPLOYEES  . "None, ";

                    }
                    else{
                        $round->ERRORS_EMPLOYEES =  $round->ERRORS_EMPLOYEES . (string) $error->EMPLOYEE_ID . " | " . $error->GIVEN_NAME . " " . $error->FAMILY_NAME . ", ";
                    }
                    if ($error->DESCRIPTION == null){
                        $round->ERRORS_NOTES =  $round->ERRORS_NOTES . "\r\n \r\n" ."None ";
                    }
                    else {
                        $round->ERRORS_NOTES =  $round->ERRORS_NOTES . "\r\n \r\n" .(string) $error->DESCRIPTION;
                    }
                    
                    
                    
                }
            }
        //csv creation 
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=' . $download_name
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        array_unshift($rounds, array_keys((array)$rounds[0]));

        $callback = function() use ($rounds) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($rounds as $row) { 
                fputcsv($FH, (array) $row);
            }
            fclose($FH);
        };

        return response()->stream($callback, 200, $headers);
        

    }

    public function downloadByError(Request $request)
    {
        $proj = $request->project;
        $off = $request->office; 
        $download_name = "";
        if (ctype_digit($request->tz)){
            $tz_off = (int) $request->tz;
        }
        else {
            $tz_off = 0; 
        }
        if ($tz_off == 0){
            $tz_off_str = '+0:00';
        }
        elseif ($tz_off > 0) {
            $tz_off_str = "+" . $tz_off . ":00";
        }
        else {
            $tz_off_str = (string) $tz_off . ":00"; 
        }
        $tz_utc = "+0:00";
        
        if (($proj == "all") && ($off == "all")){
            $download_name = "AllProjects_QcErrors.csv";
            $errors = DB::table('qc_errors AS E')
                    ->join('qc_rounds as R', function ($join) {
                            $join->on('R.batch_id', '=', 'E.batch_id');
                            $join->on('R.round_number', '=', 'E.round_number');
                    })
                    ->join('qc_batches as B', 'B.batch_id', '=', 'E.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->leftJoin('employee AS emp', 'E.employee_id', '=', 'emp.id')
                    ->leftJoin('subtasks AS st', 'E.subtask_id', '=', 'st.subtask_id')
                    ->select('E.id AS ID', 'emp.employee_id AS EMPLOYEE_ID', DB::raw("CONCAT(emp.given_name, ' ', emp.family_name) AS EMPLOYEE_NAME"),'E.subtask_id AS SUBTASK_ID', 'st.subtask_name AS SUBTASK_NAME', 'E.unit_number AS UNIT_NUMBER',
                        'E.description AS DESCRIPTION', 'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER',
                         'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME', 'tk.name AS TASK_NAME', 
                         'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 
                         'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL', DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `CREATED_AT(UTC" . $tz_off_str . ")`"))
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 
        }

        if (($proj == "all") && ($off != "all")) {
            $download_name = $off . "Projects_QcErrors.csv";
            if ($off == "PNH") $id = 1;
            if ($off == "VTE") $id = 2;
            if ($off == "NBO") $id = 3;

            $errors = DB::table('qc_errors AS E')
                     ->join('qc_rounds as R', function ($join) {
                            $join->on('R.batch_id', '=', 'E.batch_id');
                            $join->on('R.round_number', '=', 'E.round_number');
                        })
                    ->join('qc_batches as B', 'B.batch_id', '=', 'E.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->join('lu_office AS loc', 'loc.id', '=', 'pr.projects_location')
                    ->leftJoin('employee AS emp', 'E.employee_id', '=', 'emp.id')
                    ->leftJoin('subtasks AS st', 'E.subtask_id', '=', 'st.subtask_id')
                    ->select('E.id AS ID', 'emp.employee_id AS EMPLOYEE_ID', DB::raw("CONCAT(emp.given_name, ' ', emp.family_name) AS EMPLOYEE_NAME"),'E.subtask_id AS SUBTASK_ID', 'st.subtask_name AS SUBTASK_NAME', 'E.unit_number AS UNIT_NUMBER', 'E.description AS DESCRIPTION', 
                        'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER', 'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME', 'tk.name AS TASK_NAME', 'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL', DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `CREATED_AT(UTC" . $tz_off_str . ")`"))
                    ->where('pr.projects_location', $id)
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 

        }

        if ($proj != "all"){

            $errors = DB::table('qc_errors AS E')
                     ->join('qc_rounds as R', function ($join) {
                            $join->on('R.batch_id', '=', 'E.batch_id');
                            $join->on('R.round_number', '=', 'E.round_number');
                        })
                    ->join('qc_batches as B', 'B.batch_id', '=', 'E.batch_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'B.task_id')
                    ->join('projects AS pr', 'pr.id', '=', 'B.project_id')
                    ->leftJoin('employee AS emp', 'E.employee_id', '=', 'emp.id')
                    ->leftJoin('subtasks AS st', 'E.subtask_id', '=', 'st.subtask_id')
                    ->select('E.id AS ID', 'emp.employee_id AS EMPLOYEE_ID', DB::raw("CONCAT(emp.given_name, ' ', emp.family_name) AS EMPLOYEE_NAME"),'E.subtask_id AS SUBTASK_ID', 'st.subtask_name AS SUBTASK_NAME', 'E.unit_number AS UNIT_NUMBER', 'E.description AS DESCRIPTION', 
                        'B.batch_name as BATCH_NAME' , 'R.round_number AS ROUND_NUMBER', 'R.description as ROUND_DESCRIPTION','pr.name as PROJECT_NAME', 'tk.name AS TASK_NAME', 'R.sample_size AS SAMPLE_SIZE','R.target_accuracy AS TARGET_ACCURACY', 'R.units_found_in_error AS UNITS_IN_ERROR', 'R.accuracy AS ACCURACY', 'R.pass? AS PASS_FAIL', DB::raw("CONVERT_TZ(R.created_at,'" . $tz_utc ."','" . $tz_off_str ."') AS `CREATED_AT(UTC" . $tz_off_str . ")`"))
                    ->where('pr.id', $proj)
                    ->orderBy('R.batch_id', 'R.round_number')
                    ->get(); 
            $proj_name = $errors[0]->PROJECT_NAME;
            $download_name = $proj_name . "_QcErrors.csv";
        }

        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=' . $download_name
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        array_unshift($errors, array_keys((array)$errors[0]));

        $callback = function() use ($errors) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($errors as $row) { 
                fputcsv($FH, (array) $row);
            }
            fclose($FH);
        };

        return response()->stream($callback, 200, $headers);


    }

    public function weeklyReport(Request $request)
    {

        //$batches = \App\Batch::select('batchnae');
        return view('weeklyReport');
    }

    public function getBatchData(Request $request)
    {
        Log::info("\r\n getBAtchData: \r\n office:" . $request->office);
        Log::info("\r\n getBAtchData: \r\n project:" . $request->project);
        $off = $request->office;
        $proj = $request->project;

        if ($off == 'all' && $proj == 'all'){

            $batches =  DB::select(DB::raw("SELECT qb.batch_id, qb.batch_name, off.office_abbr, qb.project_id, pr.name AS project_name, qb.task_id, tk.name AS task_name, qb.unit, qb.batch_size, a.num_rounds FROM (
            (SELECT batch_id, COUNT(*) AS `num_rounds` FROM qc_rounds GROUP BY batch_id) a 
            LEFT JOIN 
            qc_batches AS qb ON qb.batch_id = a.batch_id 
            LEFT JOIN 
            projects pr
            ON qb.project_id = pr.id 
            LEFT JOIN
            lu_office off
            ON pr.projects_location = off.id
            LEFT JOIN 
            tasks tk 
            ON qb.task_id = tk.id 
            );" ));
            return response()->json($batches);
        }

        else if ($off != 'all' && $proj == 'all'){
            if ($off == 'PNH') $off_id = 1;
            if ($off == 'VTE') $off_id = 2; 
            if ($off == 'NBO') $off_id = 3; 
            Log::info("\r\n getBAtchData: \r\n office id :" . $off_id);

            $batches =  DB::select(DB::raw("SELECT qb.batch_id, qb.batch_name, off.office_abbr, qb.project_id, pr.name AS project_name, qb.task_id, tk.name AS task_name, qb.unit, qb.batch_size, a.num_rounds FROM (
            (SELECT batch_id, COUNT(*) AS `num_rounds` FROM qc_rounds GROUP BY batch_id) a 
            LEFT JOIN 
            qc_batches AS qb ON qb.batch_id = a.batch_id 
            LEFT JOIN 
            projects pr
            ON qb.project_id = pr.id 
            LEFT JOIN
            lu_office off
            ON pr.projects_location = off.id
            LEFT JOIN 
            tasks tk 
            ON qb.task_id = tk.id )
            WHERE pr.projects_location = " . $off_id . ";" ));
            return response()->json($batches);
        }

        else if ($proj != 'all'){

            $batches =  DB::select(DB::raw("SELECT qb.batch_id, qb.batch_name, off.office_abbr, qb.project_id, pr.name AS project_name, qb.task_id, tk.name AS task_name, qb.unit, qb.batch_size, a.num_rounds FROM (
            (SELECT batch_id, COUNT(*) AS `num_rounds` FROM qc_rounds GROUP BY batch_id) a 
            LEFT JOIN 
            qc_batches AS qb ON qb.batch_id = a.batch_id 
            LEFT JOIN 
            projects pr
            ON qb.project_id = pr.id 
            LEFT JOIN
            lu_office off
            ON pr.projects_location = off.id
            LEFT JOIN 
            tasks tk 
            ON qb.task_id = tk.id )
            WHERE pr.id = " . $proj . ";" ));
            return response()->json($batches);
        }
    }



    /* gets and returns the rounds associated with a batch */ 
    public function getRounds(Request $request)
    {
        $rounds = \App\qc_round::where('batch_id', $request->batch_id) 
                                ->get(); 
        return response()->json($rounds);
    }


     public function viewRound(Request $request)
    {
        $bid = $request->query('bid');
        $round_number = $request->query('rn');
        $batch = DB::table('qc_batches AS qb')
                    ->join('projects AS pr', 'pr.id', '=', 'qb.project_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'qb.task_id')
                    ->select('pr.name as project_name', 'qb.project_id', 'tk.name AS task_name', 'qb.task_id', 'qb.batch_id', 'qb.batch_name', 'qb.unit', 'qb.batch_size')
                    ->where('qb.batch_id', $bid)
                    ->get(); 

         $round = \App\Qc_round::where('batch_id', $bid)
                        ->where('round_number', $round_number)
                        ->get(); 

        $errors = DB::table('qc_errors AS qe')
                    ->join('employee AS emp', 'qe.employee_id', '=', 'emp.id')
                    ->join('subtasks AS st', 'st.subtask_id', '=', 'qe.subtask_id')
                    ->select('qe.batch_id', 'qe.round_number', 'qe.employee_id', 'emp.given_name', 'emp.family_name','qe.subtask_id', 'st.subtask_name as subtask_name', 'qe.description', 'qe.unit_number')
                    ->where('qe.batch_id', $bid)
                    ->where('qe.round_number', $round_number)
                    ->get(); 
        //return response()->json($errors);
        return view('seeround', ['batch' => $batch[0], 'round' => $round[0], 'errors' => $errors]);

    }


     public function viewRoundNoError(Request $request)
    {
        $bid = $request->query('bid');
        $round_number = $request->query('rn');
        $batch = DB::table('qc_batches AS qb')
                    ->join('projects AS pr', 'pr.id', '=', 'qb.project_id')
                    ->join('tasks AS tk', 'tk.id', '=', 'qb.task_id')
                    ->select('pr.name as project_name', 'qb.project_id', 'tk.name AS task_name', 'qb.task_id', 'qb.batch_id', 'qb.batch_name', 'qb.unit', 'qb.batch_size')
                    ->where('qb.batch_id', $bid)
                    ->get(); 

         $round = \App\Qc_round::where('batch_id', $bid)
                        ->where('round_number', $round_number)
                        ->get(); 
        Log::info("\r\n \r\n ROUND: " . serialize($round) . "\r\n \r\n ");
         
        if ($round[0]->employees != null || $round[0]->subtasks != null || $round[0]->notes != null){
            $employees  = DB::table('qc_employees AS qe')
                    ->join('employee AS emp', 'qe.employee_id', '=', 'emp.id')
                    ->select('qe.batch_id', 'qe.round_number', 'qe.employee_id', 'emp.given_name', 'emp.family_name')
                    ->where('qe.batch_id', $bid)
                    ->where('qe.round_number', $round_number)
                    ->get();

            $subtasks  = DB::table('qc_subtasks AS qs')
                    ->join('subtasks AS sub', 'qs.subtask_id', '=', 'sub.subtask_id')
                    ->select('qs.batch_id', 'qs.round_number', 'qs.subtask_id', 'sub.subtask_name')
                    ->where('qs.batch_id', $bid)
                    ->where('qs.round_number', $round_number)
                    ->get(); 
            return view('seeround_no_errors', ['batch' => $batch[0], 'round' => $round[0], 
            'employees' => $employees, 'subtasks' => $subtasks]);
        }
        
        $errors = DB::table('qc_errors AS qe')
                    ->leftJoin('employee AS emp', 'qe.employee_id', '=', 'emp.id')
                    ->leftJoin('subtasks AS st', 'st.subtask_id', '=', 'qe.subtask_id')
                    ->select('qe.batch_id', 'qe.round_number', 'qe.employee_id', 'emp.given_name', 'emp.family_name','qe.subtask_id', 'st.subtask_name as subtask_name', 'qe.description', 'qe.unit_number')
                    ->where('qe.batch_id', $bid)
                    ->where('qe.round_number', $round_number)
                    ->get(); 
        Log::info("\r\n \r\n ROUND: " . serialize($errors) . "\r\n \r\n ");
        return view('seeround', ['batch' => $batch[0], 'round' => $round[0], 'errors' => $errors]);
        

    }

    /* get the tasks associated with a given project. project id in 
    project_name field fo the request . */ 
    public function getTasks(Request $request)
    {
    	$project_name = $request->project_name; 
    	$tasks = DB::table('tasks_projects AS tp')
		    		->join('projects AS pr', 'pr.id', '=', 'tp.project_id')
		    		->join('tasks AS tk', 'tk.id', '=', 'tp.task_id')
		    		->select('pr.name', 'tp.project_id', 'tk.name AS task_name', 'tp.task_id')
		    		->where('tp.project_id', $project_name)
		    		->get(); 
        return response()->json($tasks);
    }

    /* get the projects associated with an office */ 
    public function getProjectsByOffice(Request $request)
    {
        $office_loc = $request->office; 
        if ($office_loc == 'all'){
            $projects = \App\Project::all()->orderBy('name')->get(); 
        }
        else {
             $projects = \App\Project::where('projects_location', $office_loc)->orderBy('name')->get();
        }
        return response()->json($projects);
    }



    /* get the units associated with a given task */ 
    public function getTaskUnits(Request $request)
    {
        $task_id = $request->task_id; 
        $unit = DB::table('tasks AS t')
                    ->join('references AS ref', 't.unit_type', '=', 'ref.id')
                    ->select('t.id', 'ref.name', 'ref.types', 't.unit_type')
                    ->where('ref.types', 'UNIT')
                    ->where('t.id', $task_id)
                    ->distinct()->get(); 
        return response()->json($unit);
    }

    public function getSuggestedSampleSize($batchsize){
        $sample_size = null;
        $sample_code = '';
        if ($batchsize >= 2 && $batchsize <= 8){
            $sample_code = 'A';
            $sample_size = 2;
        }
        if ($batchsize >= 9 && $batchsize <= 15){
            $sample_code = 'B';
            $sample_size = 3;
        }
        if ($batchsize >= 16 && $batchsize <= 25){
            $sample_code = 'C';
            $sample_size = 5;
        }
        if ($batchsize >= 26 && $batchsize <= 50){
            $sample_code = 'D';
            $sample_size = 8;
        }
        if ($batchsize >= 51 && $batchsize <= 90){
            $sample_code = 'E';
            $sample_size = 13;
        }
        if ($batchsize >= 91 && $batchsize <= 150){
            $sample_code = 'F';
            $sample_size = 20;
        }
        if ($batchsize >= 151 && $batchsize <= 280){
            $sample_code = 'G';
            $sample_size = 32;
        }
        if ($batchsize >= 281 && $batchsize <= 500){
            $sample_code = 'H';
            $sample_size = 50;
        }
        if ($batchsize >= 501 && $batchsize <= 1200){
            $sample_code = 'J';
            $sample_size = 80;
        }
        if ($batchsize >= 1201 && $batchsize <= 3200){
            $sample_code = 'K';
            $sample_size = 125;
        }
        if ($batchsize >= 3201 && $batchsize <= 10000){
            $sample_code = 'L';
            $sample_size = 200;
        }
        if ($batchsize >= 10001 && $batchsize <= 35000){
            $sample_code = 'M';
            $sample_size = 315;
        }
        if ($batchsize >= 35001 && $batchsize <= 150000){
            $sample_code = 'N';
            $sample_size = 500;
        }
        if ($batchsize >= 150001 && $batchsize <= 500000){
            $sample_code = 'P';
            $sample_size = 800;
        }
        if (500001 <= $batchsize ){
            $sample_code = 'Q';
            $sample_size = 2000;
        }
        return ['sample_code' => $sample_code, 'sample_size' => $sample_size];

    }

    /* submits the batch and continues to QC Round 1. */ 
    public function continueBatch(Request $request)
    {

        $task_id = (int) $request->task; 
        $project_id = (int) $request->project;
        $unit = $request->unit; 
        $batchname = $request->batchname; 
        $batchsize = (int) $request->batchsize;  
        $samples = $this->getSuggestedSampleSize($batchsize);
        $suggested_sample_size = $samples['sample_size'];
        $suggested_sample_code = $samples['sample_code'];

        $batch = \App\Batch::create(['project_id' => (int) $project_id, 
            'batch_name' => $request->batchname,
            'task_id' =>  $task_id,
            'unit' => $request->unit,
            'batch_size' => $batchsize,
            'suggested_sample_size' => $suggested_sample_size
        ]);
        Log::info("\r\n Continue batch: \r\n unit...." . $unit . "....\r\n");
        $batch_id = $batch->id;
        return view ('/continuebatch', ['project' => $request->project, 'task' => $request->task, 
       'unit' => $request->unit, 'batch_id' => $batch_id, 'suggested_sample_size' => $suggested_sample_size, 'batchsize' => $batchsize]);
    }

    /* continues on the 2nd stage of submit a QC round and to the error 
    notes page */ 
    public function firstRoundSubmit(Request $request)
    {

        $task_id = $request->task; 
        $project_id = $request->project; 
        $unit = $request->unit; 
        $subtasks = DB::table('subtasks_projects AS sp')
                    ->join('projects AS pr', 'pr.id', '=','sp.project_id')
                    ->join('tasks AS tk', 'tk.id','=', 'sp.task_id')
                    ->join('subtasks AS st', 'st.subtask_id','=', 'sp.subtask_id')
                    ->select('sp.project_id', 'sp.task_id', 'sp.subtask_id as subtask_id', 'st.subtask_name as subtask_name', 'tk.name as task_name', 'pr.name as project_name')
                    ->where('tk.id', $task_id)
                    ->where('pr.id', $project_id)
                    ->distinct()->get(); 

        $employees = DB::table('productivity AS pr')
                        ->join('employee as emp', 'pr.employee_id', '=', 'emp.id')
                        ->select(DB::raw('LTRIM(emp.family_name) as family_name, LTRIM(emp.given_name) as given_name, emp.id as id'))
                        ->where('emp.family_name', '!=', '')
                        ->where('emp.in_trash', '0')
                        ->where('pr.project_id', $project_id)
                        ->orderby(DB::raw('given_name'))
                        ->distinct()->get(); 

        //format employees to return 
        $employees_format = array();
        foreach($employees as $employee){
            $name = $employee->given_name . " " . $employee->family_name;
            $val = (string) $employee->id . "|" . $name;
            $obj = (object) array('value' => $val , 'name' => $name);
            array_push($employees_format, $obj);
        }


            return view ('/adderrornotes', ['subtasks' => $subtasks,  
        'samplesize' => $request->samplesize, 'unit' => $request->unit,
        'employees' => $employees_format, 'project_id'=>$project_id, 'task_id'=>$task_id, 'round_description' => $request->description, 
        'targetaccuracy' => $request->targetaccuracy, 'batch_id' => $request->batchid ]);
        
    }

    public function check_aql($target_aql, $sample_size){

        

        if ($target_aql >= 10.0){
            $aql = 10.0;
        }
        elseif($target_aql >= 6.5){
            $aql = 6.5;
        }
        elseif($target_aql >= 4.0){
            $aql = 4.0;
        }
        elseif($target_aql >= 2.5){
            $aql = 2.5;
        }
        elseif($target_aql >= 1.5){
            $aql = 1.5;
        }
        elseif ($target_aql >= 1.0){
            $aql = 1.0;
        }
        elseif($target_aql >= .650){
            $aql = .650;
        }
        elseif($target_aql >= .400){
            $aql = .400;
        }
        elseif($target_aql >= .250){
            $aql = .250;
        }
        elseif ($target_aql >= .150){
            $aql = .150;
        }
        elseif($target_aql >= .100){
            $aql = .100;
        }
        elseif($target_aql >= .065){
            $aql = .065;
        }
        elseif($target_aql >= .040){
            $aql = .040;
        }
        elseif ($target_aql >= .025){
            $aql = .025;
        }
        elseif($target_aql >= .015){
            $aql = .015;
        }
        elseif($target_aql >= .010){
            $aql = .010;
        }
        elseif($target_aql == 0){
            return ['ac' => 0, 're' => -1, 'aql' => 0];

        }

        #calculate sample size level. If in between levels it chooses the floor. 
        if ($sample_size <= 2){
            $ss = 2;
        }
        elseif($sample_size <= 3){
            $ss = 3; 
        }
        elseif($sample_size <= 5){
            $ss = 5; 
        }
        elseif($sample_size <= 8){
            $ss = 8; 
        }
        elseif($sample_size <= 13){
            $ss = 13; 
        }
        elseif($sample_size <= 20){
            $ss = 20; 
        }
        elseif($sample_size <= 32){
            $ss = 32; 
        }
        elseif($sample_size <= 50){
            $ss = 50; 
        }
        elseif($sample_size <= 80){
            $ss = 80; 
        }
        elseif($sample_size <= 125){
            $ss = 125; 
        }
        elseif($sample_size <= 200){
            $ss = 200; 
        }
        elseif($sample_size <= 315){
            $ss = 315; 
        }
        elseif($sample_size <= 500){
            $ss = 500; 
        }
        elseif($sample_size <= 800){
            $ss = 800; 
        }
        elseif($sample_size <= 1250){
            $ss = 1250; 
        }
        else {
            $ss = 2000;
        }


        $aql_vals = array(.01, .015, .025, .040, .065, .1, .15, .25, .4, .65, 1.0, 1.5, 2.5, 4.0, 6.5, 10.0);
        $ac_re_vals = array(
            2 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2),
            3 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2),
            5 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2),
            8 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3),
            13 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4),
            20 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6),
            32 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8),
            50 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11),
            80 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15),
            125 => array(0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22),
            200 => array(0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22),
            315 => array(0,1,0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22,21,22),
            500 => array(0,1,0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22,21,22,21,22),
            800 => array(0,1,0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22,21,22,21,22,21,22),
            1250 => array(0,1,0,1,1,2,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22,21,22,21,22,21,22,21,22),
            2000 => array(0,1,0,1,1,2,2,3,3,4,5,6,7,8,10,11,14,15,21,22,21,22,21,22,21,22,21,22,21,22,21,22)
        );

        foreach($ac_re_vals as $vals){
            Log::info("\r\n LENGTH VALS: " . count($vals));


        }

        Log::info("\r\n Target AQL: " . $target_aql);
        Log::info("\r\n AQL: " . $aql);
        Log::info("\r\n ss: " . $ss);


        $idx = array_search($aql, $aql_vals); 
        $ac_idx = $idx*2;
        $re_idx = $idx*2 + 1; 

        $ac = $ac_re_vals[$ss][$ac_idx];
        $re = $ac_re_vals[$ss][$re_idx];
        return ['ac' => $ac, 're' => $re, 'aql' => $aql];
    }

    /* submit the qc round */ 
    public function submitQC(Request $request){
        Log::info("\r\n NUM ERROR START: \r\n : " . $request->numerrors . "\r\n \r\n");
       
        $units_in_error = (int) $request->numerrors; 

        // number of units with errors 
        $error_count = (int) $request->errorcount; 
       
        // sample size 
        $total = (int) $request->samplesize;

        // accuracy of this qc round
        $accuracy = (( $total - $units_in_error ) / $total )* 100;

        //target accuracy of this qc round
        $target_accuracy = (double) $request->targetaccuracy; 

        //calculcate AQL 
        $target_aql = 100.0 - $target_accuracy;
        $sample_size = (int) $request->samplesize;
        $ac_re = $this->check_aql($target_aql, $sample_size);
        $ac = $ac_re['ac'];
        $aql = $ac_re['aql'];
        $ansi_status = true; 
        if ($units_in_error > $ac){
            $ansi_status = false; 
        }


        // if accuracy > target accuracy, pass; else fail;
        $status; 
        if ($accuracy >= $target_accuracy) $status = true;
        else $status = false;  
        $description = $request->rounddescription;

        // if no description supplied, empty string 
        if ($description == null) $description = ""; 
        
        // create a new qc round
        $round = \App\qc_round::create(['batch_id' => (int) $request->batchid, 
            'description' => $request->rounddescription,
            'sample_size' =>  (int) $request->samplesize,
            'units_found_in_error' => $units_in_error,
            'accuracy' => $accuracy,
            'pass?' => $status,
            'target_accuracy' => (double) $request->targetaccuracy,
            'ansi_status' => $ansi_status,
            'ansi_ac' => $ac,
            'ansi_aql' => $aql
        ]);
        $round_number = $round->id; 
        
        // get the batch id / name 
        $batch_id = (int) $request->batchid;
        $batch_name = \App\Batch::where('batch_id', $batch_id)
                            ->select('batch_name')
                            ->get();

        // add an error for each error not submitted
        $employees = $request->employees;
        $subtasks = $request->subtasks;
        $notes = $request->notes; 
        $unit_nums = $request->unitnumber;
        for ( $i = 0; $i < $error_count; $i++ ){
            //$subtask_key = 'subtask' . (string)$i; 
            //$description_key = 'description' . (string)$i; 
            //$employee_key = 'employee' . (string)$i; 
            //$unitnumber_key = 'unitnumber' . (string) $i;
            $employees_array = explode(",", $employees[$i]);
            foreach($employees_array as $emp){
                $id = (int) explode("|", $emp)[0];
                if ($emp == ""){
                    $id = null; 
                }
                // dont enter as an error if there is no data to enter 
                if ($emp == "" && $subtasks[$i] == 'none' && $unit_nums[$i] == "" && $notes[$i] == ""){
                    continue; 
                }
                $error = new \App\Error;
                $error->batch_id = $batch_id;
                $error->round_number = $round_number;

                if ($subtasks[$i] == 'none'){
                    $subtask_id = null;
                }
                else{
                    $subtask_id = (int) $subtasks[$i];
                }
                $error->subtask_id = $subtask_id;
                $error->employee_id = $id;
                if ($unit_nums[$i] == ""){
                    $error->unit_number = null;

                } 
                else{
                 $error->unit_number = $unit_nums[$i];
                }

                if ($notes[$i] == "") {
                    $error->description = null;
                }
                else {
                    $error->description =  $notes[$i];

                }
                $error->save(); 

            }
            
        }

        // get project and task names for the view 
        $project = \App\Project::where('id', $request->project_id)->pluck('name');
        $task = \App\Task::where('id', $request->task_id)->pluck('name');


        Log::info("\r\n UNIT IN ERROR BEFORE RETURN: \r\n : " . $units_in_error . "\r\n \r\n");
       
        //return summary view of the batch
        return view('/qc_summary', ['project' => $project, 'task' => $task, 
     'sample_size' => $request->samplesize,
            'error_count' => $units_in_error, 'accuracy' => $accuracy,
            'unit' => $request->unit,  'target_accuracy' => $target_accuracy, 
            'status' => $status, 'batch_name' => $batch_name[0]->batch_name, 'round_number' => $round_number, 'ansi_status'=> $ansi_status,
            'ansi_ac'=> $ac, 'aql' => $aql]);
    }

    /* submits the QC Round without any errors attached. Creates no entries in qc_errors table. */
    public function submitQCWithoutErrors(Request $request){
        // employees 
        $employees_string = $request->employees;
        $employees = explode(",", $employees_string);

        //error notes 
        $notes = $request->notes;   

        //subtasks 
        $subtasks = $request->input('subtasks');
        $subtasks_string = "";
        foreach($subtasks as $i=>$subtask){
            if ($i == sizeof($subtasks) - 1){
                $subtasks_string .= $subtask; 
            }
            else {
                $subtasks_string .= $subtask . ", "; 
            } 
        }

        Log::info("\r\n Employees: \r\n " . $employees_string . "....\r\n Notes: \r\n " . $notes . "\r\n Subtasks:\r\n" . serialize($subtasks) . "input: \r\n" .
            serialize($request->input()));

        // number of errors in the qc round 
        $error_count = (int) $request->errorcount; 
       
        // sample size 
        $total = (int) $request->samplesize;

        // accuracy of this qc round
        $accuracy = (( $total - $error_count ) / $total )* 100;

        //target accuracy of this qc round
        $target_accuracy = (double) $request->targetaccuracy; 

        // if accuracy > target accuracy, pass; else fail;
        $status; 
        if ($accuracy >= $target_accuracy) $status = true;
        else $status = false;  
        $description = $request->rounddescription;

        // if no description supplied, empty string 
        if ($description == null) $description = ""; 
        
        // create a new qc round
        $round = \App\qc_round::create(['batch_id' => (int) $request->batchid, 
            'description' => $request->rounddescription,
            'sample_size' =>  (int) $request->samplesize,
            'units_found_in_error' => $error_count,
            'sample_size' => (int) $request->samplesize,
            'accuracy' => $accuracy,
            'pass?' => $status,
            'target_accuracy' => (double) $request->targetaccuracy,
            'subtasks' => $subtasks_string,
            'employees'=> $employees_string, 
            'notes' => $notes
        ]);
        $round_number = $round->id; 
        
        // get the batch id / name 
        $batch_id = (int) $request->batchid;

        // add employees entry 
        foreach($employees as $employee){
            $id = (int) explode("|", $employee)[0];
            $emp = \App\qc_employee::create(['batch_id' => $batch_id, 
                'round_number' => $round_number,
                'employee_id' =>  $id
            ]);
        }
         // add subtasks entry 
        foreach($subtasks as $subtask){
            $id = (int) explode("|", $subtask)[0];
            $emp = \App\qc_subtask::create(['batch_id' => $batch_id, 
                'round_number' => $round_number,
                'subtask_id' => $id
            ]);
        }

        \App\qc_note::create(['batch_id' => $batch_id, 
                'round_number' => $round_number,
                'note' => $notes
        ]);
        Log::info("\r\n Batch ID \r\n : " . $batch_id);

        $batch_name = \App\Batch::where('batch_id', $batch_id)
                            ->select('batch_name')
                            ->get();
        Log::info("\r\n Batch Name \r\n : " . $batch_name);

        $project = \App\Project::where('id', $request->project_id)->pluck('name');
        $task = \App\Task::where('id', $request->task_id)->pluck('name');

        //return summary view of the batch
        return view('/qc_summary', ['project' => $project, 
            'task' => $task, 
            'sample_size' => $request->samplesize,
            'error_count' => $error_count, 
            'accuracy' => $accuracy,
            'unit' => $request->unit,  
            'target_accuracy' => $target_accuracy, 
            'status' => $status, 
            'batch_name' => $batch_name[0]->batch_name, 
            'round_number' => $round_number] 
        );

    }



    /* adds a qc round to an already existing batch */ 
    public function addQcToBatch(Request $request)
    {
        $bid = $request->query('bid');
        if ($bid == null) {
            return redirect()->route('/'); 
        }
        else {
            $batch = DB::table('qc_batches AS qb')
                    ->join('projects AS pr', 'qb.project_id', '=', 'pr.id')
                    ->join('tasks AS tk', 'qb.task_id','=', 'tk.id')
                    ->select('pr.name as project_name', 'pr.id as project_id', 'tk.id as task_id', 'tk.name as task_name', 'qb.batch_name', 'qb.batch_size', 'qb.batch_id', 'qb.unit')
                    ->where('batch_id', (int)$bid)
                    ->get();     
            $batchsize= $batch[0]->batch_size;
            Log::info("\r\n \r\n Batch_size : " . serialize($batchsize));
            $suggested_sample_size = $this->getSuggestedSampleSize($batchsize)['sample_size'];  
            return view ('/addqc', ['batch' => $batch[0], 'suggested_sample_size' => $suggested_sample_size]);
            
        }
    }

    


}
