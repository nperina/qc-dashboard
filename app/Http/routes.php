<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/* home page, app entry point */ 
Route::get('/','ProjectController@getHome'); 

Route::get('/getBatchData', 'ProjectController@getBatchData');

/* gets and returns rounds associated with a batch */ 
Route::post('/getRounds',  'ProjectController@getRounds');

/* sends a csv for download including the rounds of the given project */
Route::get('/download',  'ProjectController@download');

/* sends a csv for download including the rounds of the given project */
Route::get('/downloadByError',  'ProjectController@downloadByError');

/* begin qc round */ 
Route::get('/begin', function(){
	$projects = \App\Project::orderBy('name')->get();
    return view('beginbatch', ['projects' => $projects]);
});

Route::get('/submit', function () {
    return view('/submit');
});

//Route::post('/api/authenticate', 'ApiController@authenticate');


Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate', 'ApiController', ['only' => ['index']]);
    Route::post('authenticate', 'ApiController@authenticate');
    Route::post('createBatch', 'ApiController@createBatch');
    Route::post('createRound', 'ApiController@createRound');
    Route::post('createError', 'ApiController@createError');
});


/* display the round errors. needs to receive an id in the get 
request. ADD INTO HERE. */ 
//Route::get('/round/view', 'ProjectController@viewRound');

/* View round with new format, i.e. no error notes included */ 
Route::get('/round/view', 'ProjectController@viewRoundNoError');

/* View round with new format, i.e. no error notes included */ 
Route::get('/weeklyreport', 'ProjectController@weeklyReport');


/* add a qc round to an already existing batch. batch id comes in the 
get request */ 
Route::get('/batch/addqc', 'ProjectController@addQcToBatch');

/* gets the tasks associated with a project */ 
Route::post('/projectTasks',  'ProjectController@getTasks'); 

/* gets the projects associated with an office  */ 
Route::post('/getProjectsByOffice',  'ProjectController@getProjectsByOffice');

/* get the units associated with a given task. Task id in post request */
Route::post('/taskUnits',  'ProjectController@getTaskUnits'); 

/* submits the batch and continues to QC Round 1. */ 
Route::post('/continue_batch',  'ProjectController@continueBatch'); 

/* continues the QC Round to add error notes */ 
Route::post('/continue_errors',  'ProjectController@firstRoundSubmit'); 

/* submits a qc round */
/* changed from /submit_batch */
Route::post('/submit_qc',  'ProjectController@submitQC'); 
Route::post('submit_qc_without_errors', 'ProjectController@submitQCWithoutErrors');

/* adds a qc round to an already existing batch */ 
Route::get('/batch/addqc', 'ProjectController@addQcToBatch');

Route::get('/login', 'WelcomeController@index');
Route::get('/register', 'WelcomeController@index');