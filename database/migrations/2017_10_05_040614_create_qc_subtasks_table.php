<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcSubtasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('qc_subtasks', function (Blueprint $table) {
            //$table->increments('id');

		 	$table->engine = 'MyISAM';
		 	$table->increments('id');
            $table->integer('batch_id');
            $table->foreign('batch_id')->references('batch_id')->on('qc_batches');

            $table->integer('round_number');
            $table->foreign('round_number')->references('round_number')->on('qc_rounds');

            $table->integer('subtask_id')->nullable();
            $table->foreign('subtask_id')->references('subtask_id')->on('subtasks');


            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('qc_subtasks');
	}

}
