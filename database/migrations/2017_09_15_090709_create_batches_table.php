<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('qc_batches', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';
			$table->increments('batch_id');
            $table->string('batch_name');

            // must reference an existing project id 
            $table->integer('project_id');
            $table->foreign('project_id')->references('id')->on('projects');

            // must reference an existing task id 
            $table->integer('task_id');
            $table->foreign('task_id')->references('id')->on('tasks');

            // unit type from task 
            $table->string('unit');

            // number of units in the batch
            $table->integer('batch_size');

            
            // number of units in the qa sample 
            $table->timestamps('cdate');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('qc_batches');
	}

}
