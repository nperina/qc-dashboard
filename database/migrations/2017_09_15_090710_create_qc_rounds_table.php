<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcRoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('qc_rounds', function (Blueprint $table) {
            //$table->increments('id');

		 	$table->engine = 'MyISAM';
            $table->integer('batch_id');
            $table->foreign('batch_id')->references('batch_id')->on('qc_batches');

            $table->integer('round_number');
            $table->primary(['batch_id', 'round_number']);

            $table->string('description')->nullable();


            $table->integer('sample_size');
            $table->integer('units_found_in_error');

            $table->double('accuracy', 6, 2);
            // target accuracy inputted by the QA person
            $table->double('target_accuracy', 6, 2);
            // boolean for whether this round passes the QA round with accuracy 
            $table->boolean('pass?');

            // subtasks that during which errors occured - string. Actual subtasks id to be stored in qc_subtasks
            $table->string("subtasks");

            //employee ids associated with the qc round errors ---- use a new line(comma?) as a delimiter 
            $table->string("employees");

            // notes on the errors found in this --- use a new line(comma?) as a delimiter
            $table->string("notes");



            $table->timestamps();
        });

		 
        DB::statement('ALTER TABLE qc_rounds MODIFY round_number INTEGER NOT NULL AUTO_INCREMENT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('qc_rounds');
	}

}
