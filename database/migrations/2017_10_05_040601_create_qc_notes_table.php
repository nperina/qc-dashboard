<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('qc_notes', function (Blueprint $table) {
            //$table->increments('id');

		 	$table->engine = 'MyISAM';
		 	$table->increments('id');
            $table->integer('batch_id');
            $table->foreign('batch_id')->references('batch_id')->on('qc_batches');

            $table->integer('round_number');

            $table->foreign('round_number')->references('round_number')->on('qc_rounds');

            $table->string('note')->nullable();

            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('qc_notes');
	}

}
