<?php

session_start();
require 'razorflow_php/razorflow.php';
include './variables.php';


function makeLookup($data) {
  $keys = array();
  foreach($data as $key=>$vals) {
    array_push($keys, $key);
  }
  return $keys;
}

function startDate(){
  $first      = strtotime('first day of this month');
  $start_date = "20" . date('y-m-d', strtotime('-12 month', $first));

  $new_start_date = date( 'Y-m-d', strtotime('first day of this month -11 month')); 
  return $new_start_date;
}
function endDate(){
  $end_date   = "20" . date('y-m-d', strtotime('first day of this month'));

  $new_end_date   =  date('Y-m-d', strtotime('today'));
  return $new_end_date;
}


// Get all of the data for the current fiscal year. Would be good to maybe turn
// this into a sliding window, instead.
function loadData($conn) {

  $end_day = '01';
  $end_date = "20" . date('y-m', strtotime('first day of this month')) . '-' . $end_day;

  $new_end_date   =  date('Y-m-d', strtotime('today'));
  $new_start_date = date( 'Y-m-d', strtotime('first day of this month -11 month')); 
  $start_year = startDate();
  $end_date = endDate();

  
  $sql_uncontrollable_company   = sql_uncontrollable_company_f($start_year, $end_date);
  $sql_controllable_company     = sql_controllable_company_f($start_year, $end_date);
  $sql_controllable_project     = sql_controllable_project_f($start_year, $end_date);
  $sql_idle_project             = sql_idle_project_f($start_year, $end_date);
  $sql_total_productive         = sql_total_productive_f($start_year, $end_date); 
  
  /*
  $sql_uncontrollable_company   = sql_uncontrollable_company_f($new_start_date, $new_end_date);
  $sql_controllable_company     = sql_controllable_company_f($new_start_date, $new_end_date);
  $sql_controllable_project     = sql_controllable_project_f($new_start_date, $new_end_date);
  $sql_idle_project             = sql_idle_project_f($new_start_date, $new_end_date);
  $sql_total_productive         = sql_total_productive_f($new_start_date, $new_end_date); 
  */
  $result_total_productive  = dictFromResults(mysqli_query($conn, $sql_total_productive));
  $result_idle_project = dictFromResults(mysqli_query($conn, $sql_idle_project));
  $result_controllable_project = dictFromResults(mysqli_query($conn, $sql_controllable_project));
  $result_controllable_company = dictFromResults(mysqli_query($conn, $sql_controllable_company));
  $result_uncontrollable_company = dictFromResults(mysqli_query($conn, $sql_uncontrollable_company));

  $file = "data.txt";
  $write = "DATA: \r\n";
  $write .= "\r\n result_total_productive : \r\n " . serialize($result_total_productive);
  $write .= "\r\n result idle project : \r\n " . serialize($result_idle_project);
  file_put_contents($file, $write);

  return array('Productive Time'=>$result_total_productive, 
               'Project Idle Time'=>$result_idle_project,
               'Controllable Company Downtime'=>$result_controllable_company,
               'Uncontrollable Company Downtime'=>$result_uncontrollable_company,
               'Project Downtime'=>$result_controllable_project);
}

function dictFromResults($raw_results) {
  $dic = array();
  if (mysqli_num_rows($raw_results) > 0) {
    while($row = $raw_results->fetch_assoc()) {
      if ($row['MONTH'] == 0) {continue;} //work-around for month = 0 db bug
      $dic[$row['OFFICE']][$row['MONTH'] - 1][$row['PROJECT']][$row['SUB_TYPE']] = (double) $row['DURATION'];
    }
  }
  $dic = addEmpties($dic);
  $dic = addAllOffice($dic);
  return $dic;
}

function addEmpties($dict) {
  //check each of the month lengths against today's date (label length)
  return $dict;
}

// aggregate office data for the ALL tab
function addAllOffice($dict) {
  $new = $dict;
  foreach ($new[1] as $month=>$more){
    $new[4][$month] = $new[1][$month] + $new[2][$month] + $new[3][$month];
  }
  return $new;
}

// this is for the office-level view
function aggregateByOfficeAndMonth($results) {
  $dic = array();

  $write = "AGGregating:  \r\n"; 
  $file = "AggregateByOfficeAndMonth_log.txt";

  foreach($results as $office=>$months) {
    $write .= "\r \n Office: " . $office ; 
    foreach($months as $month=>$projects) {
      $total = 0;
      $write .= "\r\n " . $month ; 
      foreach($projects as $project=>$subtypes) {
        $this_project_dur = 0;
        foreach($subtypes as $sub=>$duration) {
          $this_project_dur += $duration;
        }
        $total += $this_project_dur;
      }
      $dic[$office][$month] = $total;
    }
  }
  writeToFile($write, $file); 
  return $dic;
}

//type is type[office]
//then you have month->project->sub->value
//and we want sub->value
//so we loop through month->project and add to global sub->value
//and we return that
function aggregateForOfficeBySub($months) {
  $agg_sub_vals = array();
  foreach($months as $month=>$projects) {
    foreach($projects as $project=>$subs) {
      foreach($subs as $sub=>$val) {
        if (!isset($agg_sub_vals[$sub])) {
          $agg_sub_vals[$sub] = $val;
        }
        else { 
          $agg_sub_vals[$sub] += $val; }
      }
    }
  }
  return $agg_sub_vals;
}

function getIndexForXMonthAgo($x){
  $i = date('n', strtotime('-'.$x.' month'));
  return $i - 1;
}


function getRecentProjectsForOffice($office) {
  Global $DATA;

  $compromise = array_map('clean', array_keys($DATA['Productive Time'][$office][getIndexForXMonthAgo(1)]));
  return $compromise;
}

function aggregateForOfficeByProject($months) {
  $agg_proj_vals = array();
  foreach($months as $month=>$projects) {
    foreach($projects as $project=>$subs) {
      $proj_for_month = aggregateRecursively($subs);
      if (!isset($agg_proj_vals[$project])) {
        $agg_proj_vals[$project] = $proj_for_month;
      }
      else {
        $agg_proj_vals[$project] += $proj_for_month;
      }
    }
  }
  return $agg_proj_vals;
}

function aggregateRecursively($nest, $aggregation = 0) {
  $running_below = 0;
  if (is_array($nest)) {
    foreach($nest as $level_lower) {
      $running_below += aggregateRecursively($level_lower, $aggregation);
    } 
  }
  else {
    return $nest;
  }
  return $aggregation + $running_below;
}


/*clean string*/
function clean ($s) {
  $string = mb_convert_encoding($s, 'UTF-8');
  return $string;
};

// takes an array and a string filename and overwrites file
// with the data in the array
function writeToFile($array, $file){
  $serializedData = serialize($array);
  file_put_contents($file, $serializedData);
}

// returns an array, takes a filename string
function readFromFile($file){
  $recoveredData = file_get_contents($file);
  return unserialize($recoveredData);
}




class PRSDashboard extends Dashboard {
  var $office;
  //use dashboard title to set data source for location
  public function setDashboardTitle($title) {
    parent::setDashboardTitle($title);
  }

  public function setDashboardOffice($office) {
    $this->setDashboardTitle($office);
    switch ($office) {
      case 'PNH':
        $this->office = 1;
        break;
      case 'VTE':
        $this->office = 2;
        break;
      case 'NBO':
        $this->office = 3;
        break;
      case 'ALL':
        $this->office = 4;
        break;
      default:
        $this->office = 4;
        break;
    }
  }


  private function makeLabels($i, $j) {
    $months = array("Jan", "Feb", 
                    "Mar","Apr", "May","Jun",
                    "Jul", "Aug", "Sep", "Oct", 
                    "Nov", "Dec");
    if ($i >= $j) {
    	$array_return = array_merge(array_slice($months, $i, 12), 
        array_slice($months, 0, $j + 1));
        $array_return[sizeof($array_return) - 1] = $array_return[sizeof($array_return) - 1] . " (Month to Date)";
      return  $array_return;
      //return array_merge(array_slice($months, $i, 12 - $i),
                         //array_slice($months, 0, $j));
    }
    $array_return = array_slice($months, $i, $j);
    $array_return[sizeof($array_return) - 1] = $array_return[sizeof($array_return) - 1] . " (Month to Date)";
    return $array_return;
    //return array_slice($months, $i, $j - $i);
  }

  private function getMonthIndexFromLabel($label) {
    $file = "get_month_index_from_label.txt";
    $write = "\r\n label: " . $label; 
    $write .= "\r\n label explode: " . serialize(explode(" ", $label));
    if (sizeof(explode(" ", $label)) > 1) {
      $write .= "size of greater than 1";

    }
    file_put_contents($file, $write);
    $months = array("Jan", "Feb", 
                    "Mar","Apr", "May","Jun",
                    "Jul", "Aug", "Sep", "Oct", 
                    "Nov", "Dec",);
    return array_search($label, $months);
  }
  private function getMonth($fy_index) {
    $months = array("January", "February", 
                    "March","April", "May","June",
                    "July", "August", "September", "October", 
                    "November", "December");
    return $months[$fy_index];
  }
  private function getMonthIndexFromDate($date) {
    $month = date('m', strtotime($date));
    return (int)$month - 1;
  }
  // entry point for rendering the dashboard.
  public function buildDashboard() {
    global $MONTHS;
    global $TARGETS;
    global $DATA;
    global $backlog;
    global $current;

    $start_i = $this->getMonthIndexFromDate(startDate());
    $end_i   = $this->getMonthIndexFromDate(endDate());

    $actions = readFromFile('actions');
    //$form = new FormComponent("form");
    //$form->setDimensions (8, 2);
    //$form->setCaption('Improvement Actions');
    //$form->addSelectField ("project", "Project", getRecentProjectsForOffice($this->office));
    //$form->addTextField ("directive", "Direction");

    //$tab = new TableComponent("tab");
    //$tab->setDimensions(8,4);
    //$tab->setCaption('Previous Actions');
    //$tab->addColumn('project', 'Project');
    //$tab->addColumn('action', 'Action');
    //$actions = readFromFile('actions');
    //foreach ($actions as $proj=>$proj_action) {
      //foreach($proj_action as $dob){
       //$tab->addRow($dob);
      //}
    //}

    //$form->onApplyClick(array($tab), 'handleApply', $this);
    /***********************************************************************/
    /* Monthly Hours by Category */

    // $chart_monthly = new ChartComponent('Stacked Chart');
    // $chart_monthly->setCaption('PRS Hours by Category');
    // $chart_monthly->setDimensions(8, 6);
    // $chart_monthly_opts = array('numberSuffix' => ' hours','seriesStacked' => true, 'seriesDisplayType'=>'column');

    // $chart_monthly->setLabels($this->makeLabels($start_i, $end_i));

    // foreach($DATA as $dt_type=>$office_data) {
    //   $aggregated = aggregateByOfficeAndMonth($office_data);
    //   $chart_monthly->addSeries($dt_type, $dt_type, array_values($aggregated[$this->office]), $chart_monthly_opts);
    // }
    // $chart_monthly->addDrillStep("drill_prs_by_project", $this);
    // $chart_monthly->addDrillStep("drill_prs_by_sub", $this);
    /********************************************************************************************/
    // Productive Hours as % of Available Hours (Resource Allocation) 
    $alloc = array();
    $avail = array();

    $lim = $start_i >= $end_i ? (12 - $start_i + $end_i) : ($end_i - $start_i);
    $lim = 12;  
    for ($i = 0; $i < $lim; $i++) {
      $month = $start_i + $i > 11 ? $end_i - $lim + $i + 1 : $start_i + $i;

      $alloc[] = aggregateRecursively($DATA['Productive Time'][$this->office][$month]) +
        aggregateRecursively($DATA['Project Idle Time'][$this->office][$month]) +
        aggregateRecursively($DATA['Project Downtime'][$this->office][$month]);
      $avail[] = $alloc[sizeof($alloc)-1] +
        aggregateRecursively($DATA['Controllable Company Downtime'][$this->office][$month]);
    }
    $write = "";
    $file = "aggr_log.txt";
    $chart_ratio = new ChartComponent("Allocation");
    $chart_ratio->setCaption("Productive Hours/Available Hours");
    $chart_ratio->setDimensions(12, 6);

    $chart_ratio->setLabels($this->makeLabels($start_i, $end_i));
    $labels = $this->makeLabels($start_i, $end_i);
    $percent = function($num, $denom) {
      return ((float)$num / (float)$denom) * 100.0;
      };

    $actuals = array_map($percent, aggregateByOfficeAndMonth($DATA['Productive Time'])[$this->office], $avail);
     
    $chart_ratio->addSeries('actuals', 'Actuals',
                        $actuals,
                        array('seriesDisplayType' => "column"));

    $write .= "\r\n Array actuals: \r\n " . serialize(array_map($percent, aggregateByOfficeAndMonth($DATA['Productive Time'])[$this->office], $avail));

     // change to 85 up until september and then 90 after that 
     $curr_date = new \DateTime(); 
     $sep_date = new \DateTime('09/01/2018');
     if ($curr_date >= $sep_date){
     	$chart_ratio->addSeries('target', 'Target', array_fill(0,$lim, 90), 
                          array('numberSuffix' => "%",'seriesDisplayType' => "line"));
      $series_array = array_fill(0,$lim, 90);
     }
     else {
     	$series_array = array(); 
     	if ($end_i > 8){
     		for ($i = 0; $i < $lim; $i++){
	     		if ($i >= $lim - ($end_i - 8) ){
	     			array_push($series_array, 90); 
	     		}
	     		else {
	     			array_push($series_array, 85);
	     		}
     		}
     	}
     	else {
     		for ($i = $start_i; $i < 9; $i++){
     			array_push($series_array, 85);
     		}
     		for ($i = 9; $i < $lim; $i++){
     			array_push($series_array, 90 ); 

     		}
     		for ($i = 0; $i < $end_i; $i++){
     			array_push($series_array, 90);
     		}
     	}
     	$write .= "\r\n target array: " . serialize($series_array);
     	$chart_ratio->addSeries('target', 'Target', $series_array, 
                          array('numberSuffix' => "%",'seriesDisplayType' => "line"));
     }
     

     /*$chart_ratio->addSeries('target', 'Target', array_fill(0,$lim, 90), 
                          array('numberSuffix' => "%",'seriesDisplayType' => "line"));
     */
     $chart_ratio->setYAxis('', array(
       'numberSuffix' => '%'
     ));
    $chart_ratio->addDrillStep("prs_by_category", $this);
    $chart_ratio->addDrillStep("drill_prs_by_project", $this);
    $chart_ratio->addDrillStep("drill_prs_by_sub", $this);
    $chart_ratio->addDrillStep("drill_prs_by_task", $this);
 /**********************************************************************************/
    $chart_type = new ChartComponent("PRS Downtime by Type");
    $chart_type->setCaption("FY17 to date PRS Downtime by Type");
    $chart_type->setDimensions(4, 6);
    $chart_type->setLabels(array('',
                                "Project Idle",
                                "Controllable Company DT",
                                "Uncontrollable Company DT",
                                "Project DT"));
    $chart_type->setPieValues(array( -1,
              aggregateRecursively($DATA['Project Idle Time'][$this->office]),
              aggregateRecursively($DATA['Controllable Company Downtime'][$this->office]),
              aggregateRecursively($DATA['Uncontrollable Company Downtime'][$this->office]),
              aggregateRecursively($DATA['Project Downtime'][$this->office])
              ));
    $chart_type->addDrillStep('drill_fy_pie', $this);
 /********************************************************************************************/
    $add_em_all_up = function ($vte, $pnh, $nbo) { return $vte + $pnh + $nbo;};
    $table = new TableComponent('table1');
        $table->setCaption("Raw Data");
        $table->setDimensions (4, 4);
        $table->setRowsPerPage (12);
        $table->addColumn('month', "Month");
        $table->addColumn('actuals', "Actuals");
        $table->addColumn('target', "Target");

    for ($i = 0; $i < $lim; $i++){
      $table->addRow (array("month" => $labels[$i], "actuals" => $actuals[$i], "target" => $series_array[$i]));
    }

    file_put_contents($file, $write);


    $this->addComponent($chart_ratio);
    $this->addComponent($chart_type);
     $this->addComponent ($table);

    //$this->addComponent($form);
   // $this->addComponent($tab);

    /*
    $table = new TableComponent('table1');
    $table->setCaption('Chart Data');
    $table->setDimensions(4,4);
    $table->setRowsPerPage (8);
    $table->addColumn('zone', 'Zone');
    $table->addColumn('name', 'Name');
    $table->addColumn('sales', 'Sales Amount');
    $data = array(
      array("zone" => "North", "name" => "Northern Stores", "sale" => 4000),
      array("zone" => "South", "name" => "Southern Stores", "sale" => 4500)
    );

    $table->addMultipleRows($data);

    $this->addComponent ($table);
  */

  }

  /*
  public function handleApply($source, $target, $params) {

    $c1 = $this->getComponentByID('form');
    $post = $c1->getAllInputValues();
    $proj = $post['project']['text'];
    $actions = readFromFile('actions');

    if (!isset($post['directive'])) {
      if (array_key_exists($proj, $actions)) {
        unset($actions[$proj]);
      }
      writeToFile($actions, 'actions');
      return 1;
    }
    $dire = $post['directive'];
    $tab = $this->getComponentByID('tab');
    $new = ['project'=>$proj, 'action'=>$dire];
    $tab->addRow($new);
    if (array_key_exists($proj,$actions)) {
      $actions[$proj][] = $new;
    }
    else {
      $actions[$proj] = [$new];
    }
    writeToFile($actions, 'actions');
  }*/ 

  public function handleClick ($source, $targets, $params) {

    $file = "handle_click.txt";
    $write = "\r\n " . " handling click...\r\n ";
    file_put_contents($file, $write); 

      
  }

  public function prs_by_category($source, $target, $params){
    global $DATA;
    $file = "prs_by_category_log.txt"; 
    $write = ""; 

    $start_i = $this->getMonthIndexFromDate(startDate());
    $end_i   = $this->getMonthIndexFromDate(endDate());

    $write .= "\r\n Start_i : " . $start_i; 
    $write .= "\r\n end_i : " . $end_i; 
    foreach($this->makeLabels($start_i, $end_i) as $label) {
      $write .= "\r\n label: " . $label; 
    }; 

    file_put_contents($file, $write); 

    $source->setCaption('PRS Hours by Category');
    $source_opts = array('numberSuffix' => ' hours','seriesStacked' => true, 'seriesDisplayType'=>'column');
    $source->setLabels($this->makeLabels($start_i, $end_i));
      foreach($DATA as $dt_type=>$office_data) {
        $aggregated = aggregateByOfficeAndMonth($office_data);
        $source->addSeries($dt_type, $dt_type, array_values($aggregated[$this->office]), $source_opts);
    }
  }

   public function drill_prs_by_project($source, $target, $params) {
      global $DATA;
      global $DATA_LOOKUP;

      $monthLabel = $params['label'];
      $monthInd = $this->getMonthIndexFromLabel($monthLabel);
      $assoc_key = $DATA_LOOKUP[$params['seriesIndex'] - 1]; //get key for index
      //eg "Productive Time for November"
      $source->setCaption( $assoc_key . 
                              " for " .
                              $this->getMonth($monthInd));
      $file = "prs_by_project_log.txt";
      $write = "drill_prs_by_project()"; 

      // for project downtime, break down by sub first and then by project 
      if ($assoc_key == 'Project Downtime'){
          $hour_type_index = $assoc_key; 
          $projects = $DATA[$hour_type_index]
                                [$this->office]
                                [$this->getMonthIndexFromLabel($params['drillLabelList'][0])];
          $vals = [];
          $write .= "\r\n projects: " . serialize($projects);  
          foreach($projects as $project => $project_subs){
            foreach($project_subs as $sub_type => $sub_hrs){
              if (array_key_exists($sub_type, $vals)){
                $write .= "\r\n Adding to subtype :" . $sub_type;
                $vals[$sub_type] = $vals[$sub_type] + $sub_hrs;
                $write .= "\r\n vals[$sub_type] = " . $vals[$sub_type];
              }
              else {
                $write .= "\r\n New sub type :";
                $write .= $sub_type; 
                $vals[$sub_type] = $sub_hrs; 
                $write .= "\r\n vals[$sub_type] = " . $vals[$sub_type];
              }
            }
          }
          $labels = array_keys($vals);    
        $source->setLabels($labels);
        $series_data = array_values($vals);
        $source->addSeries($series_data, array('numberSuffix' => ' hours'));
      }

      else {
          $labels = array_keys(array_values($DATA)[$params['seriesIndex'] - 1 ][$this->office][$monthInd]);

          $source->setLabels(array_map('clean', $labels));
          $series_data = array();

          foreach($labels as $i=>$label) {
            $series_data[] = (int) aggregateRecursively($DATA[$assoc_key][$this->office][$monthInd][$label]);
          }

          $source->addSeries($series_data, array('numberSuffix' => ' hours'));
      }
      file_put_contents($file, $write); 
  }

  public function drill_prs_by_task($source, $target, $params){
      $file = "drill_prs_by_task.txt";
      $write = ""; 
      $cur_caption = $source->getCaption();
      $write .= "\r\n cur_caption: " . $cur_caption; 
      $write .= "\r\n params[drillLabelList] : " . serialize($params['drillLabelList']);
      $write .= "\r\n params[label] : " . serialize($params['label']);
      file_put_contents($file, $write);




  }

  public function drill_prs_by_sub($source, $target, $params) {
    global $DATA;
    global $DATA_LOOKUP;

    $cur_caption = $source->getCaption();
    $hour_type_index = explode(' for ', $cur_caption)[0];
    // if project downtime, do different order i.e. break down first by sub and then by project 
    if ($hour_type_index == 'Project Downtime') {
      $assoc_key = $DATA_LOOKUP[4];
      $monthLabel = $params['drillLabelList'][1];
      $monthInd = $this->getMonthIndexFromLabel($monthLabel); 

      $labels = array_keys(array_values($DATA)[4][$this->office][$monthInd]);
      $sub_label = $params['label'];
      $series_data = array();
      foreach($labels as $i=>$label) {
          $subs = $DATA[$assoc_key][$this->office][$monthInd][$label];
          if (array_key_exists($sub_label, $subs)){
            $series_data[] = $subs[$sub_label];

          }
          else {
            $series_data[] = 0; 
          }
       }
       $source->addSeries($series_data, array('numberSuffix' => ' hours'));
       $source->setLabels($labels);
    }

    else {
      $labels = array_keys($DATA[$hour_type_index]
                                [$this->office]
                                [$this->getMonthIndexFromLabel($params['drillLabelList'][0])]
                                [$params['label']]);

      $source->setLabels($labels);
      $series_data = array_values($DATA[$hour_type_index]
                                [$this->office]
                                [$this->getMonthIndexFromLabel($params['drillLabelList'][0])]
                                [$params['label']]);
      $source->addSeries($series_data, array('numberSuffix' => ' hours'));
    }
  }
  public function drill_fy_pie($source, $target, $params) {
    global $DATA;

    switch ($params['label']) {
      case "Uncontrollable Company DT" :
        $source->setCaption('Uncontrollable Company by Task');
        $breakdown = aggregateForOfficeBySub($DATA['Uncontrollable Company Downtime'][$this->office]);
        break;

      case "Controllable Company DT":        
        $source->setCaption('Controllable Company by Task');
        $breakdown = aggregateForOfficeBySub($DATA['Controllable Company Downtime'][$this->office]);
        break;

      case "Project DT" :
        $source->setCaption('Project Downtime by Project');
        $breakdown = aggregateForOfficeByProject($DATA['Project Downtime'][$this->office]);
        break;

      case "Project Idle" :
        $source->setCaption('Project Idle Time by Project');
        $breakdown = aggregateForOfficeByProject($DATA['Project Idle Time'][$this->office]);
        break;
      default:
        $breakdown = array("Not Found" => 404);
    }
    $labels = array_keys($breakdown);
    $values = array_values($breakdown);
    $source->setLabels(array_map('clean', $labels));
    $source->setPieValues($values);
  }
}


// Custom Tabbed Dashboard class.
class MyDashboard extends TabbedDashboard {

  static function getHoursByProject($project_title) {
    return $project_title;
  }

  public function buildDashboard () {

    $pnh = new PRSDashboard();
    $vte = new PRSDashboard();
    $nbo = new PRSDashboard();
    $all = new PRSDashboard();

    $pnh->setDashboardOffice('PNH');
    $vte->setDashboardOffice('VTE');
    $nbo->setDashboardOffice('NBO');
    $all->setDashboardOffice('All');

     $start_date = date( 'M j Y', strtotime('first day of this month -11 month')); 
     $end_date   =  date('M j Y', strtotime('today'));
     $title = "Efficiency Report for " . $start_date . "  -  " . $end_date; 


    //$this->setTabbedDashboardTitle("Efficiency Report for FY17");
    $this->setTabbedDashboardTitle($title);
    $this->addDashboardTab($all);
    $this->addDashboardTab($pnh);
    $this->addDashboardTab($vte);
    $this->addDashboardTab($nbo);
    

  }
}

/*******************************************************************************
 * Control session data. Without this block, mysql connection and queries
 * run with every request
 * Currently unsets 'active' (bool) session variable and reloads from db
 * after 500 seconds. 
 * ****************************************************************************/
unset($_SESSION['active']);
if (!isset($_SESSION['active'])) {
  $_SESSION['active'] = true;
  $_SESSION['time']   = time();

  $conn = @mysqli_connect($servername, $username, $password, $dbname);
  if (!$conn) {
    echo "Something has gone wrong with my connection to the database. Please
    verify that you are trying to run me from a host that is whitelisted in 
    the database, for this db user. <br>

    If it's not your job to get this app to run: oops. Please contact NSD <br>
    Beep Boop, I'm a robot.";
    die("connection failed: " . mysqli_connect_error());
  }
  else {
    $DATA = loadData($conn);
    mysqli_close($conn);
    $DATA_LOOKUP = makeLookup($DATA);
    $_SESSION['DATA'] = $DATA;
    $_SESSION['DATA_LOOKUP'] = $DATA_LOOKUP;
  }
} else {
  if (($_SESSION['time'] - time()) < -500) {
    unset($_SESSION['active']);
    header("Refresh:0"); //next user interaction will update data variables
  }
  $DATA = $_SESSION['DATA'];
  $DATA_LOOKUP = $_SESSION['DATA_LOOKUP'];
}
/*************************END SESSION Handling*********************************/
$dashboard = new MyDashboard ();
$dashboard->setStaticRoot("razorflow_php/static/rf/");
$dashboard->renderStandalone ();

echo '<script src="./colorHack.js"></script>';
?>
<link rel="stylesheet" href="./style.css">