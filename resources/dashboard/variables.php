<?php
/***************************************************************************
             _   _   ___  ____________  _____ ___________ _____ 
            | | | | / _ \ | ___ \  _  \/  __ \  _  |  _  \  ___|
            | |_| |/ /_\ \| |_/ / | | || /  \/ | | | | | | |__  
            |  _  ||  _  ||    /| | | || |   | | | | | | |  __| 
            | | | || | | || |\ \| |/ / | \__/\ \_/ / |/ /| |___ 
            \_| |_/\_| |_/\_| \_|___/   \____/\___/|___/ \____/ 
                     _   _   ___  _____  _   __ _____  
                    | | | | / _ \/  __ \| | / //  ___| 
                    | |_| |/ /_\ \ /  \/| |/ / \ `--.  
                    |  _  ||  _  | |    |    \  `--. \ 
                    | | | || | | | \__/\| |\  \/\__/ / 
                    \_| |_/\_| |_/\____/\_| \_/\____/        

    This file is included in index.php. It contains all of the hardcoded 
    elements that are utilized in the logic of index.php. These are organized
    by the following blocks:
      > MySQL RDS server login details
      > SQL commands (wrapped as functions so that the date can be variable)

/***************************************************************************/
  $servername =  "dddpnhvtenbo2001.cpddxudn0k2k.us-east-1.rds.amazonaws.com";
  $username   =  "dashboard";
  $password   =  '130nqdq-m143ndfa-23kda';
  $dbname     =  "pmprms2";
/*****************************************************************************/

$TARGETS = [
            'productive_of_available'=>
              array('Productive Hours as % of Available Hours', 
                array(85,85)),
          ];
/*****************************************************************************/
	function sql_uncontrollable_company_f($start_date, $end_date) {
	 	return "SELECT 
              t.name AS PROJECT,
              s.subtask_name AS SUB_TYPE,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                   LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                LEFT JOIN tasks t ON t.id = prod.task_id
                AND (t.name LIKE '%leave%' OR t.name LIKE '%break%')
              LEFT JOIN projects p ON prod.project_id = p.id 
                 WHERE prod.end_date > '" . $start_date . "' AND
                 prod.end_date < '" . $end_date . "' AND
                 (t.name LIKE '%leave%' OR t.name LIKE '%break%')
                 AND p.name LIKE '%Company Downtime%'
                 GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE";
    }
	function sql_controllable_company_f($start_date, $end_date) {
		return "SELECT 
              t.name AS PROJECT,
              s.subtask_name AS SUB_TYPE,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                    LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                  LEFT JOIN projects p ON prod.project_id = p.id 
                LEFT JOIN tasks t ON t.id = prod.task_id
                AND (t.name LIKE '%lateness%' 
                      OR t.name LIKE '%meeting%' 
                      OR t.name LIKE '%typing practice%'
                      OR t.name LIKE '%internship%'
                      OR t.name LIKE '%training%'
                      OR t.name LIKE '%technical%'
                      OR t.name LIKE '%idle%')
              WHERE prod.end_date > '" . $start_date . "' AND
              prod.end_date < '" . $end_date . "' AND
                (t.name LIKE '%lateness%' 
                      OR t.name LIKE '%meeting%' 
                      OR t.name LIKE '%typing practice%'
                      OR t.name LIKE '%internship%'
                      OR t.name LIKE '%training%'
                      OR t.name LIKE '%technical%'
                      OR t.name LIKE '%idle%')
              AND p.name LIKE '%Company Downtime%'
              GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE";
}

	function sql_controllable_project_f($start_date, $end_date) {
		return "SELECT 
              p.name as PROJECT,
              s.subtask_name as SUB_TYPE,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                    LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                    AND (s.subtask_name LIKE '%training%' OR
                   s.subtask_name LIKE '%meeting%'  OR
                   s.subtask_name LIKE '%support%' OR 
                   s.subtask_name LIKE '%management%')
                  LEFT JOIN projects p ON prod.project_id = p.id 
                LEFT JOIN tasks t ON t.id = prod.task_id
              WHERE prod.end_date > '" . $start_date . "' AND
              prod.end_date < '" . $end_date . "'
              AND t.name LIKE '%Project Downtime%'
              AND (s.subtask_name LIKE '%training%' OR
                s.subtask_name LIKE '%meeting%'  OR
                s.subtask_name LIKE '%support%' OR 
                s.subtask_name LIKE '%management%')
              GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE";
}
	function sql_idle_project_f($start_date, $end_date) {
		return "SELECT 
              p.name AS PROJECT,
              s.subtask_name as SUB_TYPE,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                    LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                    AND s.subtask_name LIKE '%idle%'
                  LEFT JOIN projects p ON prod.project_id = p.id 
                LEFT JOIN tasks t ON t.id = prod.task_id
              WHERE prod.end_date > '" . $start_date . "' AND
              prod.end_date < '" . $end_date . "'
              AND t.name LIKE '%Project Downtime%'
              AND s.subtask_name LIKE '%idle%'
              GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE";
}
	function sql_total_productive_f($start_date, $end_date) {
		return "SELECT 
              p.name as PROJECT,
              t.name as SUB_TYPE,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                    LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                  LEFT JOIN projects p ON prod.project_id = p.id 
                LEFT JOIN tasks t ON t.id = prod.task_id
              WHERE prod.end_date > '" . $start_date . "' AND
              prod.end_date < '" . $end_date . "'
              AND (t.name NOT LIKE '%Downtime%' AND p.name NOT LIKE '%downtime%')
              GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE";
        }

  function sql_total_productive_subtask_f($start_date, $end_date) {
    return "SELECT 
              p.name as PROJECT,
              t.name as SUB_TYPE,
              s.subtask_name as SUB_TASK,
              YEAR(prod.end_date) AS YEAR,
              MONTH(prod.end_date) AS MONTH,
              p.projects_location AS OFFICE,
              SUM(ABS(ROUND(TIME_TO_SEC(TIMEDIFF(prod.end_date, prod.start_date)) / 60 / 60, 2))) AS DURATION
              FROM 
              productivity prod

                    LEFT JOIN subtasks s ON prod.subtask_id = s.subtask_id
                  LEFT JOIN projects p ON prod.project_id = p.id 
                LEFT JOIN tasks t ON t.id = prod.task_id
              WHERE prod.end_date > '" . $start_date . "' AND
              prod.end_date < '" . $end_date . "'
              AND (t.name NOT LIKE '%Downtime%' AND p.name NOT LIKE '%downtime%')
              GROUP BY OFFICE, YEAR(prod.end_date), MONTH(prod.end_date), PROJECT, SUB_TYPE, SUB_TASK";
        }

function sql_project_task_monthly_f($project_id){
  return "SELECT  proj.name, 
  YEAR(prod.end_date) AS `Year`, 
  MONTH(prod.end_date) AS `Month`, 
  t.name AS `Task`,
  SUM(prod.units_completed) AS `Units`, 
  SUM(ABS(ROUND(((TIME_TO_SEC(TIMEDIFF(prod.end_date,prod.start_date)) / 60) / 60),2))) AS `Hours` 
  FROM productivity prod 
  LEFT JOIN projects proj ON proj.id = prod.project_id 
  LEFT JOIN tasks t ON t.id = prod.task_id 
  WHERE prod.project_id = $project_id
  GROUP BY YEAR(prod.end_date),MONTH(prod.end_date), 
  t.name;";
}