@extends('layouts.app')

@section('styles')
    <link href="{{asset('/css/home.css')}}" rel="stylesheet"/>
@stop

@section('page-scripts')
    <script>
    var counter = 0;
    //window.onload = moreFieldsFun;

    $(document).ready(function (){
         var employees = {!! json_encode($employees) !!};
         var country = [{text: "Amsterdam ", value: '1' }, {text: "Cairo ", value: '2'}];
         console.log("employees:");
         console.log(employees);
         console.log("country:");
         console.log(country);

         var cities = new Bloodhound({
          initialize: false,
          local: employees,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name')
        });

        cities.initialize();

        var elt = $('#select2');
        elt.tagsinput({
              itemValue: 'value',
              itemText: 'name',
              typeaheadjs: {
                name: 'cities',
                displayKey: 'name',
                source: cities.ttAdapter()
              }
         });



    });
   

    


    </script>


@endsection

@section('content')
    
    <div class="container">
            <div class = "col-xs-12">
                <h1>QC Round - Incorrect {{ $unit }} Count </h1>
            </div>
            <div class = "col-xs-12">
                <h4> <strong> Project: </strong> {{ $subtasks[0]->project_name }} </h4>
            </div>
            <div class = "col-xs-12">
                <h4> <strong> Task: </strong> {{ $subtasks[0]->task_name }} </h4>
            </div>
            <div class = "col-xs-12">
                <h4> Please enter the following information regarding the errors found this QC Round. </h4><br>
            </div>
           
            <form method="post" action="/submit_qc_without_errors" id = "formqc">
                <!--input type="button" id="moreFields" value="+ Add Error Note" onclick="moreFieldsFun()" /-->
                    <div class="form-group" id = "batchnameform">
                        <div class = "col-xs-12">
                            <label for="batchname">Number of Incorrect {{ $unit }}s Found (required) : </label>
                        </div>
                        <div class = "col-xs-3">
                            <div class ="error-form">
                                <input type="number" class="form-control" name="errorcount" id='errorcount' required></input>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id = "batchemployeeform">

                        <div class ="col-xs-12">
                            <label for="batchname">Subtasks Where Errors Occurred (optional) : </label>
                        </div>

                        @foreach ($subtasks as $subtask)
                        <div class = "col-xs-12">
                            <div class="form-check form-check-inline error-form">
                              <label class="form-check-label">
                                &emsp; <input class="form-check-input" name='subtasks[]' type="checkbox" id="inlineCheckbox1" value="{{$subtask->subtask_id }}|{{$subtask->subtask_name}}" + ""> {{ $subtask->subtask_name }}
                              </label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    

                   

                    <div class="form-group">
                        <div class = "col-xs-12">
                            <label for="cd">Notes (optional) :</label>
                        </div>
                         <div class = "col-xs-12">
                            <p> Please add one error type/description per line. </p>
                        </div>
                        <div class = "col-xs-12">
                            <div class ="error-form">
                                <textarea class='form-control' rows="3" cols="15" name="notes" id='description'></textarea>
                            </div>
                            <div class ="error-form">
                            </div>
                        </div>
                    </div>

                     <div class="form-group" id = "batchemployeeform">
                        <div class = "col-xs-12">
                            <label for="batchname">Employees (optional) : </label>
                        </div>
                        <div class = "col-xs-12">
                            <div class ="error-form">
                                <input type="text" id="select2" name="employees">
                                <!--select name="employees" class ="form-control" multiple id="select2"></select-->
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <input type="hidden" name="batchid" id="batchid" value="{{ $batch_id }}"></input>
                    <input type="hidden" name="project_id" id="project_id" value=" {{ $project_id }} "></input>
                    <input type="hidden" name="task_id" id="task_id" value=" {{ $task_id }} "></input>
                    <input class='btn' type="button" style="float:right" value="Submit" onclick="document.getElementById('formqc').submit();" ></input>
                    <input type="hidden" name="samplesize" value="{{ $samplesize }}" id="samplesize"></input>
                    <input type="hidden" name="rounddescription" value="{{ $round_description }}" id="rounddescription"></input>
                    <input type="hidden" name="unit" value="{{ $unit }}" id="unit"></input> 
                    <input type="hidden" name="targetaccuracy" value="{{ $targetaccuracy }}" id="targetaccuracy"></input>
                </div>
            </form>
            
    </div>
        
   
    

@endsection 




