@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1> Add a QC Round </h1>
            <form action="/continue_errors" method="post">
                <h5> <b> Project: </b> {{ $batch->project_name }} </h5>
                <h5> <b> Task: </b> {{ $batch->task_name }} </h5>
                <h5> <b> Batch: </b> {{ $batch->batch_name }} </h5>
                <input type = 'hidden' name = 'project' id = 'project' value = "{{ $batch->project_id }}"></input>
                <input type = 'hidden' name = 'task' id = 'task' value = "{{ $batch->task_id }}"></input>  
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input> 
                <input type='hidden' name="unit" id = 'unit' value = " {{ $batch->unit }}"></input> 
                <input type='hidden' name="batchid" id = 'batchid' value = " {{ $batch->batch_id }}"></input> 

                <div class="form-group" id = "samplesizeform">
                    <label id = "samplesizelabel" for="batchsize">Sample Size ( # of <span style='text-transform: lowercase;' >{{ $batch->unit }}s being checked ) : </label>
                    <p> Given the current batch size {{ $batch->batch_size }}, your suggested sample size is: {{ $suggested_sample_size }}. </p>
                    <input type="number"  class="form-control" name="samplesize" min="1" id='samplesize' value='{{$suggested_sample_size}}' required>
                </div>
                <div class="form-group" id = "targetaccuracyform">
                    <label id = "targetaccuracylabel" for="batchsize">Target Accuracy ( target percentage of <span style='text-transform: lowercase;' >{{ $batch->unit }}s </span> without errors ) : </label>
                    <input type="number" step="any" class="form-control" name="targetaccuracy" min="1" id='targetaccuracy' required>
                </div>
                <div class="form-group" id = "descriptionform">
                    <label for="description">Description (optional) </label>
                    <textarea class="form-control" id="description" name="description" value="" placeholder="description"></textarea>
                </div>
                <button id="formsubmit" type="submit" class="btn btn-default" style='float:right;'> Next </button>
            </form>
        </div>
    </div>
@endsection