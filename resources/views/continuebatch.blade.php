@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*document.getElementById('qaround').onchange = function() {
        document.getElementById("descriptionform").style.display = "block";
        document.getElementById("formsubmit").style.display = "block";

    }*/ 

    function display_unit(data){
        console.log(data);
        document.getElementById('unitlabel').innerHTML = "Unit (s) : " + data[0].name;
        document.getElementById("unit").value = data[0].name;
        document.getElementById("unitform").style.display = "block";
        document.getElementById("qaroundform").style.display = "block";
        document.getElementById('batchsizelabel').innerHTML = "Batch size ( # of " + data[0].name + 's ) :' ;
        document.getElementById("batchsizeform").style.display = "block";
        document.getElementById("descriptionform").style.display = "block";
        document.getElementById("formsubmit").style.display = "block";

        document.getElementById('samplesizelabel').innerHTML = "QA sample size ( # of " + data[0].name + 's ) :' ;
        document.getElementById("samplesizeform").style.display = "block";

        document.getElementById('targetaccuracylabel').innerHTML = "Target Accuracy ( % - age of " + data[0].name.toLowerCase() + 's  without errors ) :' ;
        document.getElementById("targetaccuracyform").style.display = "block";



    }   


});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1>New Batch - QC Round 1 </h1>
            <form action="/continue_errors" method="post">
               
                <input type = 'hidden' name = 'project' id = 'project' value = "{{ $project }}"></input>
                <input type = 'hidden' name = 'task' id = 'task' value = "{{ $task }}"></input>  
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input> 
                <input type='hidden' name="unit" id = 'unit' value = "{{ $unit }}"></input> 
                <input type='hidden' name="batchid" id = 'batchid' value = "{{ $batch_id }}"></input> 
                <div class="form-group" id = "samplesizeform">
                    <label id = "samplesizelabel" for="batchsize">QC Sample Size ( # of <span style='text-transform: lowercase;' >{{ $unit }}s being checked ) : </label>
                    <p> Given the current batch size {{ $batchsize }}, your suggested sample size is: {{ $suggested_sample_size }}. </p>
                    <input type="number" class="form-control" name="samplesize" min="1" id='samplesize' value='{{$suggested_sample_size}}' required>
                </div>
                <div class="form-group" id = "targetaccuracyform">
                    <label id = "targetaccuracylabel" for="batchsize">Target Accuracy ( target percentage of <span style='text-transform: lowercase;' >{{ $unit }}s </span> without errors ) : </label>
                    <input type="number" step="any" class="form-control" name="targetaccuracy" min="1" id='targetaccuracy' required>
                </div>
                <div class="form-group" id = "descriptionform">
                    <label for="description">QC Round Description (for example: "qc round after subtask one" or "qc round after proofreading" ): </label>
                    <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>
                </div>
                <button id="formsubmit" type="submit" class="btn btn-default" style='float:right;'> Next </button>
            </form>
        </div>
    </div>
@endsection