@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


     function alter_tasks(data){
        console.log('data:');
        console.log(data);
        taskSelect = document.getElementById('task');
        $("#task").empty();
        taskSelect.options[taskSelect.options.length] = new Option('--select an option--', '-1');
        taskSelect.options[taskSelect.options.length - 1].selected = true;
        taskSelect.options[taskSelect.options.length - 1].disabled = true;
        data.forEach(function(task){
            taskSelect.options[taskSelect.options.length] = new Option(task.task_name, '' + task.task_id);

        });
        document.getElementById("taskform").style.display = "block";
     }


     function alter_projects(data){
        console.log('data:');
        console.log(data);
        projectSelect = document.getElementById('project');
        $("#project").empty();
        projectSelect.options[projectSelect.options.length] = new Option('--select an option--', '-1');
        projectSelect.options[projectSelect.options.length - 1].selected = true;
        projectSelect.options[projectSelect.options.length - 1].disabled = true;
        data.forEach(function(project){
            projectSelect.options[projectSelect.options.length] = new Option(project.name, '' + project.id);

        });
     }


     document.getElementById('office').onchange = function() {
        office_val = $('#office').val();
        if (office_val != 'na'){
            console.log("office val:");
            console.log(office_val);
            formdata = {
                "office": office_val, 
            }
             $.ajax({
                type: "POST",
                url: '/getProjectsByOffice',
                dataType: 'json',
                data: formdata,
                success: alter_projects,
                error: function (jqXHR, exception) {
                        console.log(jqXHR);
                }
            });
        }
    };

     document.getElementById('project').onchange = function() {
        project_name = $('#project').val();
        console.log($('meta[name="csrf-token"]').attr('content'));
        formdata = {
            "project_name": project_name, 
            "id": 5
        }

        // ajax request to get tasks associated with project 
        console.log(project_name);
        $.ajax({
                type: "POST",
                url: '/projectTasks',
                dataType: 'json',
                data: formdata,
                success: alter_tasks,
                error: function (jqXHR, exception) {
                        console.log(jqXHR);
                }
        });
    };

    /*document.getElementById('qaround').onchange = function() {
        document.getElementById("descriptionform").style.display = "block";
        document.getElementById("formsubmit").style.display = "block";

    }*/ 

    function display_unit(data){
        console.log(data);
        document.getElementById('unitlabel').innerHTML = "Unit (s) : " + data[0].name;
        document.getElementById('batchsizelabel').innerHTML = "Batch Size:  (# of " + data[0].name.toLowerCase() + "s)";
        document.getElementById("unit").value = data[0].name;
        document.getElementById("unitform").style.display = "block";
        document.getElementById("batchnameform").style.display = "block";
        document.getElementById("batchsizeform").style.display = "block";
       
        document.getElementById("formsubmit").style.display = "block";



    }
    //get task unit type 
    document.getElementById('task').onchange = function() {
        task = $('#task').val();
        console.log(task);

        formdata = {
            "task_id": task
        }

        // ajax request to get tasks associated with project 
        console.log(project_name);
        $.ajax({
                type: "POST",
                url: '/taskUnits',
                dataType: 'json',
                data: formdata,
                success: display_unit,
        });
    };   


});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1> New Batch </h1>
            <form action="/continue_batch" method="post">
                <div class="form-group">
                    <label for="project">Office</label>
                    <select class="form-control" id="office" name='office'>
                        <option selected value='all'> All </option>
                        
                        <option value="1">Phnom Penh</option>
                        <option value="2">Vientiane</option>
                        <option value="3">Nairobi</option>
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="project">Project</label>
                    <select class="form-control" id="project" name='project' required>
                        <option disabled selected value> -- select an option -- </option>
                        @foreach ($projects as $project)
                            <option value=" {{$project->id }}"> {{ $project->name }} </option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group" style="display:none" id = "taskform">
                    <label for="task">Task</label>
                    <select class="form-control" id="task" name='task' required>
                    <option disabled selected value> -- select an option -- </option>
                    </select>
                </div>
                 <div class="form-group" style="display:none" id = "unitform">
                    <label id = "unitlabel" for="unit">Unit:</label>
                    <input type="hidden" class="form-control" name="unit" id='unit' required>
                </div>
                <div class="form-group" style = "display: none; " id = "batchnameform">
                    <label for="batchname">Batch Name (for example: "batch 1" or "batch due Oct 30th"): </label>
                    <input class="form-control" name="batchname" id='batchname' required> </input>
                </div>
                <div class="form-group" style = "display: none; " id = "batchsizeform">
                    <label id = "batchsizelabel" for="batchsize">Batch Size : </label>
                    <input type="number" class="form-control" name="batchsize" min="1" id='batchsize' required>
                </div>
               
                <button style="display:none; float: right;" id="formsubmit" type="submit" class="btn btn-default"> Next </button>
            </form>
        </div>
    </div>
@endsection