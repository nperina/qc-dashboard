@extends('layouts.app')

@section('page-scripts')
    <script>
    var counter = 0;
    var newFields;

    function deleteNotes(e) {
        console.log("this.parentNode.parentNode.parentNode:");
        var form = e.parentNode.parentNode.parentNode;
        
        //note_number = parseInt(e.parentNode.parentNode.id.split("-")[1]);
        console.log("note number: ");
        //console.log(note_number);
        e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
        counter--; 
        
        document.getElementById('errorcount').value = counter.toString(); 
        //loop through other children notes
        var notes = document.getElementsByClassName('note');
        for (i = 0; i < notes.length; i++){
            var note = notes[i];
            var num = i + 1; 
            note.id = "note-" + num;
            console.log("note:") 
            console.log(note); 
            console.log("h4");
            console.log(note.getElementsByTagName('h4'));
            note.getElementsByTagName('h4')[0].innerHTML = "Error Note " + num; 
            console.log("subtask-select");
            var select = note.getElementsByClassName('subtask-select')[0];
            select.setAttribute('name', 'subtasks[' + (num - 1) + '][]');


        }
        /*
         
        //html = newFields.getElementsByTagName('h4')[0].innerHTML;
        //newFields.getElementsByTagName('h4')[0].innerHTML = 'Error Note ' + counter.toString();
        */

    }

    function moreFieldsFun() {
        counter++;
        document.getElementById('errorcount').value = counter.toString(); 
        document.getElementById('readroot').childNodes[0];

        newFields = document.getElementById('readroot').cloneNode(true);
        html = newFields.getElementsByTagName('h4')[0].innerHTML;
        newFields.getElementsByTagName('h4')[0].innerHTML = html + counter; 
        newFields.id = 'note-' + counter;
        newFields.className = "note";
        newFields.style.display = 'block';
        var newField = newFields.childNodes;
        for (var i=0;i<newField.length;i++) {

            var theName = newField[i].name;
            console.log("newfield[i]");
            console.log(newField[i]);
            console.log("The name:");
            console.log(theName);
            if (newField[i].id == 'batchemployeeform'){
                var style_div = newField[i].childNodes[3];
                var input_parent = style_div.childNodes[1].childNodes[1];
                var new_id = "empselect" + counter; 
                input_parent.id = new_id;

            }
            if (newField[i].tagName == 'DIV' || newField[i].tagName == 'div'){
                var childs = newField[i].childNodes;
                for (var j = 0; j < childs.length;j++){
                    childs[j].name = childs[j].name + counter; 
                }
            }
            /*
            if (newField[i].id == "selectgroup"){
                select_div = newField[i].getElementsByTagName('div')[1];
                select = select_div.getElementsByTagName('select')[0];
                console.log("select:");
                console.log(select);
                select.name = "subtasks[" + (counter - 1) + "][]";

            }
            */
            if (theName){
                newField[i].name = theName + counter;
            }
        }
        var insertHere = document.getElementById('writeroot');
        console.log("new fields:");
        console.log(newFields);
        insertHere.parentNode.insertBefore(newFields,insertHere);

        var employees = {!! json_encode($employees) !!};
         var country = [{text: "Amsterdam ", value: '1' }, {text: "Cairo ", value: '2'}];
         console.log("employees:");
         console.log(employees);
         console.log("country:");
         console.log(country);

         var cities = new Bloodhound({
          initialize: false,
          local: employees,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name')
        });

        cities.initialize();
        var elt = $('#' + new_id);
        elt.tagsinput({
              itemValue: 'value',
              itemText: 'name',
              typeaheadjs: {
                name: 'cities',
                displayKey: 'name',
                source: cities.ttAdapter()
              }
         });
    }

     $(document).ready(function (){
         var employees = {!! json_encode($employees) !!};
         var country = [{text: "Amsterdam ", value: '1' }, {text: "Cairo ", value: '2'}];
         console.log("employees:");
         console.log(employees);
         console.log("country:");
         console.log(country);

         var cities = new Bloodhound({
          initialize: false,
          local: employees,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name')
        });

        cities.initialize();
        /*
        var elt = $('#select2');
        elt.tagsinput({
              itemValue: 'value',
              itemText: 'name',
              typeaheadjs: {
                name: 'cities',
                displayKey: 'name',
                source: cities.ttAdapter()
              }
         });
         */



    });
   

    //window.onload = moreFieldsFun;

    </script>
@endsection

@section('content')
    
    <div class="container">

            <div class = "col-xs-12">
                <h1>QC Round - Incorrect {{ $unit }} Count </h1>
            </div>
            <div class = "col-xs-12">
                <h4> <strong> Project: </strong> {{ $subtasks[0]->project_name }} </h4>
            </div>
            <div class = "col-xs-12">
                <h4> <strong> Task: </strong> {{ $subtasks[0]->task_name }} </h4>
            </div>
            
            <form method="post" action="/submit_qc">
                <div class = "col-xs-12">
                <h4> How many incorrect <span style="text-transform: lowercase;" > {{ $unit }}s </span> were found during this QC round? </h4>
                </div>
                <div class = "col-xs-3">
                    <input class="form-control" type="number" value="0" name="numerrors">
                </div> 
                <div class = "col-xs-12" style = "margin-top: 20px;">
                <h4> Please add one error note per error that you wish to save. </h4>
                </div>

                <span id="writeroot"></span>
                <div class="col-xs-12">
                <button class="btn" style="margin-top:10px;" type="button" onclick='moreFieldsFun()'>+ Add Error Note </button>
                <!--input type="button" id="moreFields" value="+ Add Error Note" onclick="moreFieldsFun()" /-->
                </div>
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <input type="hidden" name="errorcount" id="errorcount" value=0></input>
                    <input type="hidden" name="batchid" id="batchid" value="{{ $batch_id }}"></input>
                    <input type="hidden" name="project_id" id="project_id" value=" {{ $project_id }} "></input>
                    <input type="hidden" name="task_id" id="task_id" value=" {{ $task_id }} "></input>
                    <input class='btn' type="submit" style="float:right; margin-bottom:20px; margin-top:20px;" value="Submit"></input>
                    <input type="hidden" name="samplesize" value="{{ $samplesize }}" id="samplesize"></input>
                    <input type="hidden" name="rounddescription" value="{{ $round_description }}" id="rounddescription"></input>
                    <input type="hidden" name="unit" value="{{ $unit }}" id="unit"></input> 
                    <input type="hidden" name="targetaccuracy" value="{{ $targetaccuracy }}" id="targetaccuracy"></input>
                </div>
            </form>
            
  
        
    <div id="readroot" style="display: none; ">
        <div class = "col-xs-12" style = "margin-top: 10px;">
            <h4 name="errorcount"> Error Note </h4> 
        </div>
        <div class = "col-xs-12">
            <a style="cursor: pointer" onclick="deleteNotes(this);"> Delete </a></br>
        </div>
        
        <div class="form-group">
                <div class = "col-xs-12">
                <label for="cd" id = 'desclabel'>  {{$unit}} number: </label>
                </div>
                <div class = "col-xs-2" style='margin-bottom: 10px;'>
                <textarea class='form-control' rows="1" cols="15" name="unitnumber[]" id='unitnumber' value="none"></textarea>
                </div>
            
        </div>
        <div class="form-group" id="selectgroup">
            <div class="col-xs-12">
                <label for="subtask">Subtask where the error occurred:  </label>
            </div>
            <div class = "col-xs-6" style='margin-bottom: 10px;'>
                <select class="form-control subtask-select" id="sel1" name='subtasks[]'>
                    <option selected value="none"> -- no subtask selected -- </option>
                    @foreach ($subtasks as $i=>$subtask)
                        <option value="{{$subtask->subtask_id}}">{{ $subtask->subtask_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
       

        <div class="form-group" id = "batchemployeeform">
            <div class = "col-xs-12">
                <label for="batchname">Employees responsible for the error: </label>
            </div>
            <div class = "col-xs-12" style='margin-bottom: 10px;'>
                <div class ="error-form">
                    <input type="text" id="select2" name="employees[]">
                                                   <!--select name="employees" class ="form-control" multiple id="select2"></select-->
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class = "col-xs-12">
            <label for="cd">Notes:</label>
            </div>
            <div class = "col-xs-12" style="margin-bottom: 30px;">
            <textarea class='form-control' rows="3" cols="15" name="notes[]" id='description'></textarea>
            </div>
        </div>
    </div>
    </div>
    

@endsection 




