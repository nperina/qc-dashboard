@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1> QC Round Summary </h1> 
            <h3> <b> Project: </b> {{ $batch->project_name }} </h3>
            <h3> <b> Task: </b> {{ $batch->task_name }} </h3>
            <h3> <b> Batch Name: </b> {{ $batch->batch_name }} </h3>
            <h3> <b> QC Round: </b> {{ $round->round_number }} </h3>
            <h4> <b> Sample Size: </b> {{ $round->sample_size }} <b> </h4> 
            <h4> <b> Incorrect {{ $batch->unit }}s Found: </b> {{ $round->units_found_in_error }}</h4>
            <h4> <b> Accuracy: </b> {{ $round->accuracy }} </h4>
            <h4> <b> Target Accuracy: </b> {{ $round->target_accuracy }} </h4>
            @if ($round['pass?'])
            <h4> <b> Status: </b> Pass </h4>
            @else
            <h4> <b> Status: </b> Fail </h4>
            @endif

            <br>
            <h3> Subtasks: </h3>
            @foreach ($subtasks as $subtask)
                <h4> {{$subtask->subtask_name }} </h4>
            @endforeach

            <h3> Employees: </h3>
            @foreach ($employees as $employee)
                <h4> {{$employee->employee_id }} - {{$employee->given_name }} {{$employee->family_name }}</h4>
            @endforeach

            <h3> Notes: </h3> 
                <h4> {{$round->notes}} </h4>
          


            
           
        </div>
    </div>
@endsection