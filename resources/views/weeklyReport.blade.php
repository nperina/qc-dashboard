<?php 
require (app_path() . '\..\resources\dashboard\index.php');
require (app_path() . '\..\resources\dashboard\variables.php');
?>

<html>
	<head>
		<title>Laravel</title>
		<link rel="stylesheet" href="../../dashboard/razorflow_php/static/rf/css/razorflow.min.css"/>
    	<script src="../../dashboard/razorflow_php/static/rf/js/jquery.min.js" type="text/javascript"></script>
    	<script src="../dashboard/razorflow_php/static/rf/js/razorflow.wrapper.min.js" type="text/javascript"></script>
    	<script src="../dashboard/razorflow_php/static/rf/js/razorflow.devtools.min.js" type="text/javascript"></script>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	</head>
	<body>
	<iframe width="600" height="450" src="https://datastudio.google.com/embed/reporting/1Xn9vtMDRtqPvylLUxVN4X9IWUo7pYAGp/page/VF9J" frameborder="0" style="border:0" allowfullscreen></iframe>
	<iframe width="600" height="450" src="https://datastudio.google.com/embed/reporting/1MQwyYHnKKdjmHeH7cOIBUWcGGnnB2vdl/page/EJTK" frameborder="0" style="border:0" allowfullscreen></iframe>	
	<br>
	<?php
    $db = new MyDashboard ();
    $db->renderEmbedded();
    ?>
	</body>
</html>
