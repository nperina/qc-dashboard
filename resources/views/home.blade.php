@extends('layouts.app')

@section('styles')
    <link href="{{asset('/css/home.css')}}" rel="stylesheet"/>
@stop


@section('page-scripts')
    <script>
    //window.onload = moreFieldsFun;
    var batches = {!! $batches !!};
    var table;
    console.log("Date time offset:")
    var d = new Date()
    var tz = d.getTimezoneOffset() / 60 * -1; 
    console.log(tz); 

    function make_ajax (bid, callback) {
        
    }
    //inner row table format 
    function format ( d, cb ) {
    // `d` is the original data object for the row
        // ajax request to get tasks associated with project 
        formdata = {
            "batch_id": d.batch_id
        }
        $.ajax({
                type: "POST",
                url: '/getRounds',
                dataType: 'json',
                data: formdata,
                success: function (data) {
                    cb(d.unit, data); 
                }
        });
    }

     function fillTable() {
        var proj_val = $("#project-select").val();
        var off_val = $("#office-select").val();
        $.ajax({
            url: '/getBatchData',
            data: {
                "office": off_val, 
                "project" : proj_val
            },
            dataType: "json", 
            error: function(jqxhr, type, exc){
                console.log('error:');
                console.log(type);
            },
            success: function(batches){
                console.log(batches); 
                /* fill in data table */ 
                table.clear();
                table.rows.add(batches).draw();





            }

        });
    }


    $(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    console.log("batches: ");
    console.log(batches);
    var projects = batches.map(function(batch) {
        return {id: batch.project_id, name: batch.project_name }
    }); 
    projects_unique = [];
    ids = [];
    projects.forEach(function(project){
        if (ids.indexOf(project.id) == -1){
            ids.push(project.id);
            projects_unique.push(project);
        }

    })
    console.log("projects unique..");
    console.log(projects_unique);

    var $el = $("#project-select");
    $el.empty(); // remove old options
    $el.append($("<option></option>")
         .attr("value", 'all').text('All Projects'));
    $.each(projects_unique, function(i, project) {
    //console.log(project.name);
      $el.append($("<option></option>")
         .attr("value", project.id).text(project.name));
    });

   
   



    table = $('#example').DataTable( {
        "aaData": batches,
        "dom": 'Bfrtip',
        "buttons": ['excel'],
        "columns": [
                {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                },
                {"data": "project_name"},
                {"data": "office_abbr"},
                {"data": "task_name" },
                {"data": "batch_name" },
                {"data": "batch_size" },
                {"data": "num_rounds"},
                { "data": "batch_id",
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a href='/batch/addqc?bid="+oData.batch_id+"'> <span class='glyphicon glyphicon-plus-sign'></span> New QC Round </a>");
                    }
                }
        ]

    } );

    $('#office-table-filter').on('change', function(){

       table.search(this.value).draw();  
       var off = this.value; 
       if (off == 'PNH'){
            var result = batches.filter(function( obj ) {
                    return obj.office_abbr == off;
            });
            console.log("result:");
            console.log(result);
       } 
    });


    $('#office-select').on('change', function(){

       var off = this.value; 
       if (off == 'all'){
            var projects = batches.map(function(batch) {
                        return {id: batch.project_id, name: batch.project_name }
            }); 

            projects_unique = [];
            ids = [];
            projects.forEach(function(project){
                if (ids.indexOf(project.id) == -1){
                    ids.push(project.id);
                    projects_unique.push(project);
                }

            })
            var $el = $("#project-select");
            $el.empty(); // remove old options
            $el.append($("<option></option>")
                    .attr("value", 'all').text('All Projects'));
            $.each(projects_unique, function(i, project) {
                    $el.append($("<option></option>")
                        .attr("value", project.id).text(project.name));
            });

       }
       else {
            var result = batches.filter(function( obj ) {
                    return obj.office_abbr == off;
            });

            var projects = result.map(function(batch) {
                        return {id: batch.project_id, name: batch.project_name }
            });

            projects_unique = [];
            ids = [];
            projects.forEach(function(project){
                if (ids.indexOf(project.id) == -1){
                    ids.push(project.id);
                    projects_unique.push(project);
                }

            }) 
    
            var $el = $("#project-select");
            $el.empty(); // remove old options
            $el.append($("<option></option>")
                    .attr("value", 'all').text('All Projects'));
            $.each(projects_unique, function(i, project) {
                    
                    $el.append($("<option></option>")
                        .attr("value", project.id).text(project.name));
            });
       }
    });

    function get_html(unit, rounds) {

        html = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; width:95%; margin: auto;">'+
        '<thead>' +
        '<tr>'+
            '<td>Round Number</td>'+
            '<td>Sample Size</td>'+
            '<td>Incorrect ' + unit +'s </td>'+
            '<td>Target Accuracy </td>'+
            '<td>Accuracy</td>'+
            '<td>Status</td>'+
            '<td>ANSI Status</td>'+
            '<td>Description</td>'+
            '<td>View</td>'+
        '</thead'+
        '<tbody>'+
        '</tr>';
        rounds.forEach(function(round){
            html = html +
            '<tr>'+
                    '<td>'+ round.round_number + '</td>'+
                    '<td>'+ round.sample_size +'</td>'+
                    '<td>'+ round.units_found_in_error +'</td>'+
                    '<td>'+ round.target_accuracy +'</td>'+
                    '<td>'+ round.accuracy +'</td>';
                    
            if (round['pass?']) html = html + '<td>Pass</td>';
            else html = html + '<td>Fail</td>';
            if (round['ansi_status'] == 1) html = html + '<td>Pass</td>';
            else if (round['ansi_status'] == 0) html = html + '<td>Fail</td>';
            else if (round['ansi_status'] == null) html = html + '<td>n/a</td>';
            html = html + 
                     '<td>'+ round.description +'</td>'+
                     '<td><a href = "/round/view?bid='+ round.batch_id + '&rn=' + round.round_number +'" target="_blank" > <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> </a> </td>';
            '</tr>';
        });
        
        html = html + '</table>';
        return html; 



    }

    $('#download').on('click', function(){
        var office_val = $('#office-select').val();
        var project_val = $('#project-select').val();
        var url = "/download?project=" + project_val + "&office=" + office_val + "&tz=" + tz; 
        window.open(url); 
    }); 
    $('#downloadErrors').on('click', function(){
        var office_val = $('#office-select').val();
        var project_val = $('#project-select').val();
        var url = "/downloadByError?project=" + project_val + "&office=" + office_val + "&tz=" + tz; 
        window.open(url); 
    }); 

    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            format(row.data(), function(unit, rounds) {
                row.child(get_html(unit, rounds)).show();
                tr.addClass('shown'); 


            });
           
        }
    } );


} );
    </script>
@endsection

@section('content')
    
    <div class="container">
            <h1> QC Tool Home</h1>
            
            <h3 style = "margin-bottom: 15px;"> Batches </h3> 
            <h4 style = "margin-bottom: 25px;"> <a href ="/begin"> <span class='glyphicon glyphicon-plus-sign'></span> QC a New Batch </a> </h4> 

            <button id="download" class = 'btn' style = "margin-bottom: 15px;"> Download Rounds </button>
            <button id="downloadErrors" class = 'btn' style = "margin-bottom: 15px;"> Download Errors </button>
            <!--h4 style = "margin-bottom: 25px;"> <a href ="/begin"> <span class='glyphicon glyphicon-download'></span> Download CSV </a> </h4-->

            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn">
                    <select class="form-control" name="args2" id='office-select'>
                    <option value="all">All Offices</option>
                    <option>PNH</option>
                    <option>VTE</option>
                    <option>NBO</option>
                    </select>
                </span>
                <span class="input-group-btn">
                    <select class="form-control" name="args" id="project-select">
                    <option>All Projects</option>
                    </select>
                </span>
                <span class="input-group-btn">
                    <button id="searchbtn" type="submit" class="form-control" onclick="fillTable(); "><span class="glyphicon glyphicon-search"></span></button>
                </span>
              </div>
            </div>
            <div id='export'></div>
        <br>
        <table id="example" class="display" width="100%">
            <thead> 
                <tr>
                <th></th>
                <th>Project</th>
                <th>Office</th>
                <th>Task</th>
                <th>Batch Name</th>
                <th>Batch Size</th>
                <th># of QC Rounds</th>
                <th>Add QC Round</th>
                </tr>
            </thead>

        </table>




    </div>
    

@endsection 




