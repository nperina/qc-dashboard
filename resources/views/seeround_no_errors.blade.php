@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1> QC Round Summary </h1> 
            <h3> <b> Project: </b> {{ $batch->project_name }} </h3>
            <h3> <b> Task: </b> {{ $batch->task_name }} </h3>
            <h3> <b> Batch Name: </b> {{ $batch->batch_name }} </h3>
            <h3> <b> QC Round: </b> {{ $round->round_number }} </h3>
            <br> 
            <h4> <b> Sample Size: </b> {{ $round->sample_size }} <b> </h4> 
            <h4> <b> Incorrect {{ $batch->unit }}s Found: </b> {{ $round->units_found_in_error }}</h4>
            <h4> <b> Accuracy: </b> {{ $round->accuracy }} </h4>
            <h4> <b> Target Accuracy: </b> {{ $round->target_accuracy }} </h4>
            @if ($round['pass?'])
            <h4> <b> Status: </b> Pass </h4>
            @else
            <h4> <b> Status: </b> Fail </h4>
            @endif
            @if ($round['ansi_status'] == 1)
            <h4> <b> ANSI Status: </b> Pass </h4>
            @elseif ($round['ansi_status'] == null)
            <h4> <b> ANSI Status: </b> n/a </h4>
            @elseif ($round['ansi_status'] == 0)
            <h4> <b> ANSI Status: </b> Fail </h4>
            @endif
            @if ($round['ansi_ac'] != null)
            <h4> <b> ANSI Acceptance Number: </b> {{$round['ansi_ac']}} </h4>
            @else
            <h4> <b> ANSI Acceptance Number: </b> n/a </h4>
            @endif
            <h4> <b> Subtasks: </b> 
            @foreach ($subtasks as $i=>$subtask)
                @if ($i == sizeof($subtasks) - 1)
                    {{$subtask->subtask_name}}
                @else
                    {{$subtask->subtask_name}}, 
                @endif 

            @endforeach
            </h4 >

            <h4> <b> Employees: </b>
            @foreach ($employees as $i=>$employee)
                @if ($i == sizeof($employees) - 1)
                    {{$employee->employee_id }} - {{$employee->given_name }} {{$employee->family_name }}
                @else 
                    {{$employee->employee_id }} - {{$employee->given_name }} {{$employee->family_name }}, 
                @endif
            @endforeach
            </h4>

            <h4> <b> Notes:  </b>
            {{$round->notes}} </h4>
          


            
           
        </div>
    </div>
@endsection