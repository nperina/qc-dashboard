@extends('layouts.app')

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

    // You can access the value of your select field using the .val() method
   
</script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1> QC Round Summary: </h1> 
            <h4> <b> Project: </b> {{ $batch->project_name }} </h4>
            <h4> <b> Task: </b> {{ $batch->task_name }} </h4>
            <h4> <b> Batch Name: </b> {{ $batch->batch_name }} </h4>
            <h4> <b> QC Round: </b> {{ $round->round_number }} </h4>
            <h4> <b> Description: </b> {{ $round->description }} </h4>
            <h4> <b> Sample Size: </b> {{ $round->sample_size }} <b> </h4> 
            <h4> <b> Incorrect {{ $batch->unit }}s Found: </b> {{ $round->units_found_in_error }}</h4>
            <h4> <b> Accuracy: </b> {{ $round->accuracy }} </h4>
            <h4> <b> Target Accuracy: </b> {{ $round->target_accuracy }} </h4>
            @if ($round['pass?'])
            <h4> <b> Status: </b> Pass </h4>
            @else
            <h4> <b> Status: </b> Fail </h4>
            @endif
            <h1> Error Notes: </h1>
            @foreach ($errors as $error)
                @if(is_null($error->unit_number))
                <h4> <b> {{ $batch->unit }} Number:</b> None </h4>
                @else
                <h4> <b> {{ $batch->unit }} Number:</b> {{$error->unit_number }} </h4>
                @endif
                @if(is_null($error->subtask_name))
                <h4> <b> Subtask in Which Errors Occurred :</b> None </h4>
                @else 
                <h4> <b> Subtask in Which Errors Occurred :</b> {{$error->subtask_name }} </h4>
                @endif
                @if(is_null($error->description))
                <h4> <b> Description: </b> None </h4>
                @else
                <h4> <b> Description: </b> {{$error->description }} </h4>
                @endif
                @if(is_null($error->given_name) and is_null($error->family_name) )
                <h4> <b> Employee: </b> None </h4>
                @else
                <h4> <b> Employee: </b> {{ $error->given_name }} {{ $error->family_name }}</h4>
                @endif
                <br>
            @endforeach
            
           
        </div>
    </div>
@endsection