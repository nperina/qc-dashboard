@extends('layouts.app')

@section('page-scripts')
    <script>
    //window.onload = moreFieldsFun;
    
       // $('#homebutton').click(function(){
         //   console.log('clicked');
          //  window.location.href = '/'; 

        //});

    </script>
@endsection

@section('content')
    
    <div class="container">
            <h1> {{ $batch_name }} </h1>
            <h3> Summary </h3> 
            <!-- h4> <b> Project: </b> {{ $batch->project_name }} </h4>
            <h4> <b> Task: </b> {{ $batch->task_name }} </h4>
            <h4> <b> Batch Name: </b> {{ $batch->batch_name }} </h4>
            <h4> <b> QC Round: </b> {{ $round->round_number }} </h4>
            <h4> <b> Sample Size: </b> {{ $round->sample_size }} <b> </h4> 
            <h4> <b> Incorrect {{ $batch->unit }}s Found: </b> {{ $round->units_found_in_error }}</h4>
            <h4> <b> Accuracy: </b> {{ $round->accuracy }} </h4>
            <h4> <b> Target Accuracy: </b> {{ $round->target_accuracy }} </h4>
            @if ($round->status)
            <h4> <b> Status: </b> Pass </h4>
            @else
            <h4> <b> Status: </b> Fail </h4>
            @endif

            <h3> Incorrect {{ $batch->unit }}s Found: </h3>
            @foreach ($errors as $error)
                <h4> <b> {{ $batch->unit }} Number/ID:</b> {{error->unit_number }} </h4>
                <h4> <b> Subtask in Which Errors Occurred :</b> {{error->subtask_name }} </h4>
                <h4> <b> Description of Errors: </b> {{error->description }} </h4>
                <h4> <b> Employee who Performed this Subtask: </b> {{ error->employee }} </h4>
                <br>
            @endforeach
            <button id='homebutton' class='btn btn-default'> Home </button-- >
    </div>
    

@endsection 




