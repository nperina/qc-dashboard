@extends('layouts.app')

@section('page-scripts')
    <script>
    //window.onload = moreFieldsFun;
    
        $('#homebutton').click(function(){
            console.log('clicked');
            window.location.href = '/'; 

        });

    </script>
@endsection

@section('content')
    
    <div class="container">
            <h1> QC Round Submitted. </h1>
            <h3> Summary </h3> 
            <h4> <b> Project: </b> {{ $project }} </h4>
            <h4> <b> Task: </b> {{ $task }} </h4>
            <h4> <b> Batch Name: </b> {{ $batch_name }} </h4>
            <h4> <b> QC Round: </b> {{ $round_number }} </h4>
            <h4> <b> Sample Size: </b> {{ $sample_size }} <b> </h4> 
            <h4> <b> Incorrect {{ $unit }}s Found: </b> {{ $error_count }}</h4>
            <h4> <b> Accuracy: </b> {{ $accuracy }} </h4>
            <h4> <b> Target Accuracy: </b> {{ $target_accuracy }} </h4>
            @if ($status == 1)
            <h4> <b> Status: </b> Pass </h4>
            @else
            <h4> <b> Status: </b> Fail </h4>
            @endif
            @if ($ansi_status == 1)
            <h4> <b> ANSI Standard Status (AQL = {{ $aql }}): </b> Pass </h4>
            @else
            <h4> <b> ANSI Standard Status (AQL = {{ $aql }}):</b> Fail </h4>
            @endif
            <h4> <b> ANSI Standard Acceptance Number: </b> {{$ansi_ac}} </h4>
            <button id='homebutton' class='btn btn-default'> Home </button>
    </div>
    

@endsection 




